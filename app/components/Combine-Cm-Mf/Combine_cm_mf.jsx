"use client";

import React from "react";
import Benefit from "./Benefit";
import Mona_Flow from "./Mona_Flow";
import Device from "./Device";

export default function Combine_cm_mf() {
  return (
    <div className="combine-cm-mf">
      <Benefit />
      <Mona_Flow />
      <Device />
    </div>
  );
}
