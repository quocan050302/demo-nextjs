"use client";
import React, { useEffect } from "react";
import Script from "next/script";
import Image from "next/image";

export default function Device() {
  useEffect(() => {
    const deviceList = document.querySelectorAll(".device-dot-item");
    if (deviceList.length > 0) {
      deviceList.forEach((item) => {
        const deviceInner = item.querySelector(".device-content-inner");
        const deviceDot = item.querySelector(".device-dot");
        if (window.innerWidth <= 576) {
          deviceList[0].classList.add("active");
        } else {
          deviceList[1].classList.add("active");
        }
        deviceInner.classList.remove("active");
        deviceDot.addEventListener("click", (e) => {
          for (let a = 0; a < deviceList.length; a++) {
            deviceList[a].classList.remove("active");
          }
          item.classList.toggle("active");
        });
      });
    }
  }, []);
  return (
    <div class="device txt-white">
      <div class="container">
        <div class="sec-com-tt txt-center">
          Trang bị những{" "}
          <span class="txt-nhtq">thiết bị tiên tiến, studio đa dạng </span>
          <br />
          <div class="sure">
            {" "}
            <span>
              nhằm đảm bảo{" "}
              <span class="txt-bg txt-bg-ani bg-nhtq add-active-js quality-txt">
                <div class="txt">chất lượng </div>
              </span>{" "}
              dịch vụ tốt nhất
            </span>
          </div>
        </div>
      </div>
      <div class="device-wrap">
        <div class="device-content">
          <div class="device-list">
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/gimbal.png"
                            src="./assets/images/dich-vu-chup-profile/gimbal.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Gimbal RS3 Pro
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/mayanh.png"
                            src="./assets/images/dich-vu-chup-profile/mayanh.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Sony A7 - IV{" "}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/flycam.png"
                            src="./assets/images/dich-vu-chup-profile/flycam.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Flycam DJI Mavic 3 Pro
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/lens.png"
                            src="./assets/images/dich-vu-chup-profile/lens.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Lens Sony 24-70 GM
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/den.png"
                            src="./assets/images/dich-vu-chup-profile/den.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Đèn Godox AD600
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/lens-02.png"
                            src="./assets/images/dich-vu-chup-profile/lens-02.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Lens Sony 16-35 GM
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="device-dot-item">
              <div class="device-dot">
                <div class="device-dot-inner">
                  <div class="device-content-item">
                    <div class="device-content-inner">
                      <div class="device-border">
                        <img
                          src="./assets/images/dich-vu-chup-profile/device-border.png"
                          alt="img-err"
                        />
                      </div>
                      <div class="device-content-image">
                        <div class="device-img">
                          <img
                            class="img opti-image cir"
                            data-src="./assets/hq-images/dich-vu-chup-profile/mayanh.png"
                            src="./assets/images/dich-vu-chup-profile/mayanh.png"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div class="device-image-txt fw-600 txt-center">
                        Sony A7 - IV
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="device-image">
            {" "}
            {/* <img
              class="img opti-image"
              data-src="./assets/hq-images/quay-tvc/device-img.jpg"
              src="./assets/images/quay-tvc/device-img.jpg"
              alt="img-err"
            /> */}
            <Image
              width={5000}
              height={5000}
              quality={100}
              className="img"
              priority
              src="/assets/hq-images/quay-tvc/device-img.jpg"
              alt="img-err"
            />
          </div>
        </div>
      </div>
      {/* <Script id="device">{`
        const deviceList = document.querySelectorAll(".device-dot-item");
        if (deviceList.length > 0) {
          deviceList.forEach((item) => {
            const deviceInner = item.querySelector(".device-content-inner");
            const deviceDot = item.querySelector(".device-dot");
            if (window.innerWidth <= 576) {
        
              deviceList[0].classList.add("active");
            }
            else {
              deviceList[1].classList.add("active");
            }
            deviceInner.classList.remove("active");
            deviceDot.addEventListener("click", (e) => {
              for (let a = 0; a < deviceList.length; a++) {
                deviceList[a].classList.remove("active");
              }
              item.classList.toggle("active");
            });
          });
        }
        
      `}</Script> */}
    </div>
  );
}
