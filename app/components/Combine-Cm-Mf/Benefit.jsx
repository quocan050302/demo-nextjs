"use client";
import Image from "next/image";

export default function Benefit() {
  return (
    <section className="sec-com benefit">
      <div className="container">
        <h2 className="sec-com-tt txt-center commit-head">
          Để có được những bức hình, thước phim <br className="br-first" /> chất
          lượng <br className="br-second" />
          <span className="txt-bg txt-bg-ani bg-nhtq add-active-js mg-auto active">
            <span className="txt">
              {" "}
              chúng tôi đã đầu tư rất nhiều vào dịch vụ
            </span>
          </span>
        </h2>
        <div className="benefit-container">
          <div className="benefit-content--left">
            <div className="slide-text-show swiper">
              <ul className="swiper-wrapper wrapper-container">
                <li className="swiper-slide wrapper-inner-item">
                  <div className="slide-item">
                    <span className="slide-item-txt txt-lms">
                      Khảo sát{" "}
                      <span className="slide-item-txt-inner">
                        địa điểm, quy mô <br /> nhà máy, xí nghiệp
                      </span>
                    </span>
                  </div>
                </li>
                <li className="swiper-slide wrapper-inner-item">
                  <div className="slide-item">
                    <span className="slide-item-txt txt-software">
                      Nghiên cứu kỹ{" "}
                      <span className="slide-item-txt-inner">
                        khách hàng <br />
                        của bạn đang cần gì
                      </span>
                    </span>
                  </div>
                </li>
                <li className="swiper-slide wrapper-inner-item">
                  <div className="slide-item">
                    <span className="slide-item-txt txt-green">
                      Đầu tư chất xám{" "}
                      <span className="slide-item-txt-inner">
                        cho kịch <br />
                        bản chụp phù hợp, logic
                      </span>
                    </span>
                  </div>
                </li>
                <li className="swiper-slide wrapper-inner-item">
                  <div className="slide-item">
                    <span className="slide-item-txt txt-nhtq">
                      Cung cấp giải pháp{" "}
                      <span className="slide-item-txt-inner">
                        chụp <br /> cho bạn
                      </span>
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div className="benefit-content--right">
            <div className="swiper-banner swiper">
              <div className="swiper-wrapper">
                <div className="swiper-slide">
                  <div className="banner-img banner-img-01">
                    <div className="img-1">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-1-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-1-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-1.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-1-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-1-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-1.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-1.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-2">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-2-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-2-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-2.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-2-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-2-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-2.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-2.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-3">
                      {" "}
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-3-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-3-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-3.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-3-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-3-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-3.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-3.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-4">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-4-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-4-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-4.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-4-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-4-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-4.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-4.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="banner-img banner-img-02">
                    <div className="img-1">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-5-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-5-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-5.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-5-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-5-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-5.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-5.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-2">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-6-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-6-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-6.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-6-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-6-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-6.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-6.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-3">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-7-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-7-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-7.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-7-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-7-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-7.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-7.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="banner-img banner-img-03">
                    <div className="img-1">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-8-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-8-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-8.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-8-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-8-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-8.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-8.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-2">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-9-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-9-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-9.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-9-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-9-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-9.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-9.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-3">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-10-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-10-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-10.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-10-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-10-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-10.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-10.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="banner-img banner-img-04">
                    <div className="img-1">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-11-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-11-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-11.png 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-11-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-11-md.png 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-11.png 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-11.png"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-2">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-12-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-12-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-12.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-12-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-12-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-12.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-12.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-3">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-13-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-13-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-13.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-13-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-13-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-13.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-13.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                    <div className="img-4">
                      <img
                        className="opti-image"
                        data-srcset="./assets/hq-images/chup-anh-xi-nghiep/commit-img-14-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-14-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/commit-img-14.jpg 961w"
                        srcSet="./assets/images/chup-anh-xi-nghiep/commit-img-14-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/commit-img-14-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/commit-img-14.jpg 961w"
                        src="./assets/images/chup-anh-xi-nghiep/commit-img-14.jpg"
                        sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                        alt="img-err"
                        loading="lazy"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
