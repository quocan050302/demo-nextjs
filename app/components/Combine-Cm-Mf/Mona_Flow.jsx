"use client";
import React, { useEffect } from "react";
import Script from "next/script";
import Image from "next/image";
import moodImg from "../../../public/assets/hq-images/chup-anh-xi-nghiep/mood-board.jpg";

export default function Mona_Flow() {
  useEffect(() => {
    function observeAndAddActive() {
      const configObserver = {
        rootMargin: "-50px -50px -50px -50px",
        threshold: [0, 0.25, 0.75, 1],
      };

      const addActives = document.querySelectorAll(".add-active-mess");

      const observerAddActives = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.intersectionRatio > 0) {
            entry.target.classList?.add("active");
            // Kiểm tra nếu đã thêm lớp 'active' thì ngừng quan sát
            if (!entry.target.classList.contains("observed")) {
              entry.target.classList?.add("observed");
              observerAddActives.unobserve(entry.target);
            }
          } else {
            entry.target.classList.remove("active");
          }
        });
      }, configObserver);

      addActives.forEach((ele) => {
        observerAddActives.observe(ele);
      });
    }

    // Gọi hàm để bắt đầu quan sát và thêm lớp 'active'
    observeAndAddActive();
  }, []);
  return (
    <section className="mona-flow txt-white">
      <div className="step step-2">
        <div className="arrow-down-wrapper">
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
        </div>
        <div className="container">
          <h2 className="fw-700 txt-center mona-flow-custom--head">
            Xây dựng{" "}
            <span className="txt-bg txt-bg-ani bg-digital add-active-js mg-auto active head-custom">
              <span className="txt"> concept hoàn hảo</span>
            </span>{" "}
            bám sát thực tế
          </h2>
          <div className="research-decor research-decor-1">
            {" "}
            <img
              className="opti-image"
              data-srcset="./assets/hq-images/chup-anh-xi-nghiep/built-01-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/built-01-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/built-01.jpg 961w"
              srcSet="./assets/images/chup-anh-xi-nghiep/built-01-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/built-01-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/built-01.jpg 961w"
              src="./assets/images/chup-anh-xi-nghiep/built-01.jpg"
              sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
              alt="img-err"
              loading="lazy"
            />
          </div>
          <div className="research-decor research-decor-2">
            <img
              className="opti-image"
              data-srcset="./assets/hq-images/chup-anh-xi-nghiep/built-02-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/built-02-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/built-02.jpg 961w"
              srcSet="./assets/images/chup-anh-xi-nghiep/built-02-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/built-02-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/built-02.jpg 961w"
              src="./assets/images/chup-anh-xi-nghiep/built-02.jpg"
              sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
              alt="img-err"
              loading="lazy"
            />
          </div>
          <div className="research-decor research-decor-3">
            <img
              className="opti-image"
              data-srcset="./assets/hq-images/chup-anh-xi-nghiep/built-03-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/built-03-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/built-03.jpg 961w"
              srcSet="./assets/images/chup-anh-xi-nghiep/built-03-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/built-03-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/built-03.jpg 961w"
              src="./assets/images/chup-anh-xi-nghiep/built-03.jpg"
              sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
              alt="img-err"
              loading="lazy"
            />
          </div>
          <div className="research-decor research-decor-4 add-active-mess">
            <img
              src="./assets/images/chup-anh-xi-nghiep/built-04.png"
              alt="img-err"
            />
          </div>
          <div className="research-decor research-decor-5 add-active-mess">
            <img
              src="./assets/images/chup-anh-xi-nghiep/built-05.png"
              alt="img-err"
            />
          </div>
          <div className="reaseach-main">
            <div className="mona-wrap-img">
              {" "}
              <Image
                quality={100}
                placeholder="blur"
                src={moodImg}
                alt="img-err"
              />
              {/* <img
                className="opti-image"
                data-srcset="./assets/hq-images/chup-anh-xi-nghiep/mood-board-sm.jpg 576w, ./assets/hq-images/chup-anh-xi-nghiep/mood-board-md.jpg 960w, ./assets/hq-images/chup-anh-xi-nghiep/mood-board.jpg 961w"
                srcSet="./assets/images/chup-anh-xi-nghiep/mood-board-sm.jpg 576w, ./assets/images/chup-anh-xi-nghiep/mood-board-md.jpg 960w, ./assets/images/chup-anh-xi-nghiep/mood-board.jpg 961w"
                src="./assets/images/chup-anh-xi-nghiep/mood-board.jpg"
                sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
                alt="img-err"
                loading="lazy"
              /> */}
            </div>
            <div className="reaseach-item">
              <div className="avt-mess add-active-mess">
                <img
                  className="avt-author"
                  src="./assets/images/tkw-never-stop/avt-atuan.png"
                  alt="img-err"
                />
                <div className="mess-test add-active-mess">
                  Chief Customer Officer
                </div>
                <div className="mess add-active-mess">
                  <div className="icon">
                    <img
                      src="./assets/images/tkw-never-stop/growth 1.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="txt">
                    Cùng bạn lên ý tưởng, cách chụp phù hợp với quy mô doanh
                    nghiệp
                  </div>
                </div>
              </div>
            </div>
            <div className="reaseach-item">
              <div className="avt-mess add-active-mess">
                <img
                  className="avt-author"
                  src="./assets/images/tkw-never-stop/avt-ahy.png"
                  alt="img-err"
                />
                <div className="mess-test add-active-mess">
                  Head of Website Department
                </div>
                <div className="mess add-active-mess style-pri">
                  <div className="icon">
                    <img
                      src="./assets/images/tkw-never-stop/smile 1.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="txt">
                    Chuẩn bị đạo cụ, máy móc, setup thiết bị, ánh sáng
                  </div>
                </div>
              </div>
            </div>
            <div className="reaseach-item">
              <div className="avt-mess add-active-mess">
                <img
                  className="avt-author"
                  src="./assets/images/tkw-never-stop/avt-acino.png"
                  alt="img-err"
                />
                <div className="mess-test add-active-mess">
                  Art & Creative Director
                </div>
                <div className="mess add-active-mess style-pri">
                  <div className="icon">
                    <img
                      src="./assets/images/tkw-never-stop/like 1.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="txt">
                    Đầu tư chất xám cho từng khâu chuẩn bị
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="arrow-down-wrapper">
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
          <div className="arrow-group">
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
            <div className="arrow">
              <img
                src="./assets/images/tkw-never-stop/arrow-down.svg"
                alt="img-err"
              />
            </div>
          </div>
        </div>
      </div>
      <Script id="mess">
        {`
            
              
        `}
      </Script>
    </section>
  );
}
