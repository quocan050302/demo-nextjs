"use client";

export default function Company() {
  return (
    <section className="sec-company sec-com">
      <div className="sec-com-header company-header">
        <div className="container">
          <div className="sec-com-tt fw-700 txt-center company-title">
            {" "}
            Các dịch vụ khác tại
            <span className="logo">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/logo-02.png"
                alt="img-err"
              />
            </span>
          </div>
        </div>
      </div>
      <div className="sec-com-content company-content">
        <div className="container">
          <div className="company-list d-flex f-ctn">
            <div className="col col-4">
              <div className="company-item company-item-1">
                <div className="company-item-txt fw-700">
                  DỊCH VỤ SẢN XUẤT VIDEO
                </div>
                <div className="item-content">
                  <div className="content">
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/quay-phim-gioi-thieu-doanh-nghiep/"
                    >
                      <div className="item-txt">
                        Dịch vụ quay video doanh nghiệp
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="javascript:void(0)"
                    >
                      <div className="item-txt">
                        Dịch vụ quay video showcase lãnh đạo
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/dich-vu-quay-tvc-quang-cao/"
                    >
                      <div className="item-txt">
                        Dịch vụ quay TVC quảng cáo{" "}
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/chup-anh-su-kien/"
                    >
                      <div className="item-txt">Dịch vụ quay TVC sự kiện</div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="javascript:void(0)"
                    >
                      <div className="item-txt">
                        Dịch vụ quay phóng sự, podcast
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/dich-vu-quay-flycam/ "
                    >
                      <div className="item-txt">Dịch vụ quay flycam shot </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                  </div>
                </div>
                <div className="item-img">
                  <img
                    src="./assets/images/dich-vu-chup-profile/company-item-1.png"
                    alt="img-err"
                  />
                  <div className="blur">
                    <img
                      src="./assets/images/tkw-never-stop/company-blur-1.png"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-4">
              <div className="company-item company-item-2">
                <div className="company-item-txt fw-700">DỊCH VỤ CHỤP ẢNH</div>
                <div className="item-content">
                  <div className="content">
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/chup-anh-profile-cong-ty/"
                    >
                      <div className="item-txt">
                        Dịch vụ chụp ảnh công ty, nhà xưởng
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/chup-anh-doanh-nhan/"
                    >
                      <div className="item-txt">
                        Dịch vụ chụp ảnh doanh nhân
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="https://mona.media/chup-anh-san-pham/"
                    >
                      <div className="item-txt">Dịch vụ chụp ảnh sản phẩm</div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="javascript:void(0)"
                    >
                      <div className="item-txt">
                        Dịch vụ chụp ảnh resort, khu tham quan
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href=" ttps://mona.media/chup-hinh-360-do-chup-hinh-vr-tour-flycam-4k/"
                    >
                      <div className="item-txt">Dịch vụ chụp ảnh 360</div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                    <a
                      className="item"
                      target="_blank"
                      href="javascript:void(0)"
                    >
                      <div className="item-txt">
                        Dịch vụ chụp ảnh kiến trúc, nội thất
                      </div>
                      <div className="item-icon">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/icon-arrow.png"
                          alt="img-err"
                        />
                      </div>
                    </a>
                  </div>
                </div>
                <div className="item-img">
                  <img
                    src="./assets/images/dich-vu-chup-profile/company-item-02.png"
                    alt="img-err"
                  />
                  <div className="blur">
                    <img
                      src="./assets/images/tkw-never-stop/company-blur-2.png"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col col-4">
              <div className="company-item company-item-3">
                <div className="item-content">
                  <div className="company-item-content">
                    <div className="company-item-tt">QUAY CHỤP</div>
                    <div className="company-item-sale">
                      <div className="txt">GIẢM 10%</div>
                    </div>
                  </div>
                </div>
                <div className="item-img style-pri">
                  <div className="inner">
                    <img
                      src="./assets/images/dich-vu-chup-profile/company-item-33.png"
                      alt="img-error"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
