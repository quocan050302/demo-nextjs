"use client";

import React from "react";

export default function Factor() {
  return (
    <section className="sec-com factor-san-pham">
      <img
        className="factor-mask"
        src="./assets/images/chup-anh-san-pham/mask-03.png"
        alt="img-err"
      />
      <div className="container">
        <h2 className="txt-center txt-title">
          <div className="txt-head">
            Đầu tư <span className="txt-nhtq fw-700"> hình ảnh chất lượng</span>
          </div>
          <div className="txt-sub-head">
            Nâng cao{" "}
            <span className="txt-bg txt-bg-ani bg-nhtq add-active-js mg-auto">
              <div className="txt"> mọi điểm tiếp xúc </div>
            </span>{" "}
            của người mua{" "}
          </div>
        </h2>
        <div className="txt-desc">
          <span className="txt-green fw-700">93% </span>người dùng xem hình ảnh
          là <span className="txt-green fw-700"> yếu tố cần thiết </span> để
          quyết định <br />
          mua hàng
        </div>
        <div className="factor-content">
          <div className="factor--inner-01 factor--inner">
            <div className="factor-container-01 factor-container">
              <img
                className="khung-01 khung-img"
                src="./assets/images/chup-anh-san-pham/khung-01.png"
                alt="img-err"
              />
              <img
                className="khung-inner-01 khung-inner"
                src="./assets/images/chup-anh-san-pham/khung-inner-01.png"
                alt="img-err"
              />
              <img
                className="khung-inner-02 khung-inner"
                src="./assets/images/chup-anh-san-pham/khung-inner-02.png"
                alt="img-err"
              />
              <img
                className="khung-inner-03 khung-inner"
                src="./assets/images/chup-anh-san-pham/khung-inner-03.png"
                alt="img-err"
              />
              <img
                className="khung-inner-04 khung-inner"
                src="./assets/images/chup-anh-san-pham/khung-inner-04.png"
                alt="img-err"
              />
              <div className="btn-ani">
                <div className="link-mua-hang">
                  <span>Mua hàng</span>
                </div>
              </div>
              <div className="wrapper-cursor">
                <div className="cursor-route">
                  <div className="route">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="165"
                      height="115"
                      viewBox="0 0 165 115"
                      fill="none"
                    >
                      <path d="M198 80L1 1" stroke="black"></path>
                    </svg>
                  </div>
                  <div className="cursor-orange">
                    <img
                      src="./assets/images/chup-anh-san-pham/cursor-orange.png"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="inner-text">
              <span className="txt">Kích thích thị giác, </span>
              <br /> tạo động lực mua sắm
            </div>
          </div>
          <div className="factor--inner-02 factor--inner">
            <div className="factor-container-02 factor-container">
              <img
                className="khung-02 khung-img"
                src="./assets/images/chup-anh-san-pham/khung-02.png"
                alt="img-err"
              />
              <img
                className="khung-inner-01 khung-inner"
                src="./assets/images/chup-anh-san-pham/khung-inner-01.png"
                alt="img-err"
              />
              <div className="five-star">
                <img
                  className="star-img"
                  src="./assets/images/chup-anh-san-pham/star.png"
                  alt="img-err"
                />
              </div>
              <div className="cursor-route">
                <div className="route">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="128"
                    height="156"
                    viewBox="0 0 99 153"
                    fill="none"
                  >
                    <path d="M1 155.5L127.5 1" stroke="black"></path>
                  </svg>
                </div>
                <div className="cursor-blue">
                  <img
                    src="./assets/images/chup-anh-san-pham/cursor-blue.png"
                    alt="img-err"
                  />
                </div>
              </div>
            </div>
            <div className="inner-text">
              <span className="txt txt-nhtq">Nâng cao </span> trải nghiệm
              <br /> người dùng
            </div>
          </div>
          <div className="factor--inner-03 factor--inner">
            <div className="factor-container-03 factor-container">
              <img
                className="khung-03 khung-img"
                src="./assets/images/chup-anh-san-pham/khung-03.png"
                alt="img-err"
              />
              <div className="sub-container--left">
                <img
                  className="khung-inner-01 khung-inner"
                  src="./assets/images/chup-anh-san-pham/khung-inner-05.png"
                  alt="img-err"
                />
              </div>
              <div className="sub-container--right">
                <span className="monthly">Bán trong tháng</span>
                <span className="product">2.509+ sản phẩm</span>
                <span className="increase">+ 2.000 tăng thêm</span>
                <div className="keywords-loader">
                  <div className="loader add-active-js">
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item gradient"></div>
                    <div className="loader-item gradient"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item gradient"></div>
                    <div className="loader-item"></div>
                    <div className="loader-item gradient"></div>
                    <div className="loader-item gradient"></div>
                  </div>
                </div>
              </div>
              <div className="result-chart-item count-block">
                <div className="c-progress-circle" data-percentage="60">
                  <svg className="c-progress-circle__svg">
                    <defs>
                      <linearGradient id="gradient">
                        <stop offset="0%" stopColor="#1092F3" />
                        <stop offset="100%" stopColor="#AC43FF" />
                      </linearGradient>
                    </defs>
                    <circle
                      className="c-progress-circle__bar"
                      r="30"
                      cx="50%"
                      cy="50%"
                      data-percentage="60"
                      stroke="url(#gradient)"
                    ></circle>
                  </svg>
                  <div className="c-progress-content">
                    <div className="progress-tt fw-700">
                      {" "}
                      <span className="number counter-up" data-count="200">
                        0
                      </span>
                      %
                    </div>
                    <div className="progress-desc">doanh thu</div>
                  </div>
                </div>
                <div className="result-chart-info">
                  <div className="chart-list">
                    <div className="chart-item">
                      <div className="txt">Trước</div>
                      <div className="txt">$$$$</div>
                    </div>
                    <div className="chart-item">
                      <div className="txt">Sau</div>
                      <div className="txt txt-linear fw-700">20 lần</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="chart-img--left">
                <img
                  src="./assets/images/chup-anh-san-pham/factor-content-01.png"
                  alt="img-err"
                />
              </div>
              <div className="chart-img-right">
                <img
                  src="./assets/images/chup-anh-san-pham/factor-content-02.png"
                  alt="img-err"
                />
              </div>
            </div>
            <div className="inner-text">
              <span className="txt">Thúc đẩy và tăng hiệu quả</span>
              <br /> quảng cáo trên mạng xã hội
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
