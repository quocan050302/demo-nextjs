"use client";
import Script from "next/script";
import { useEffect } from "react";
import Image from "next/image";

export default function Module() {
  useEffect(() => {
    const count = document.querySelectorAll(".count-js");
    if (count.length > 0) {
      const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

      let today = new Date(),
        lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0), // Get the last day of the current month
        birthday = new Date(
          lastDay.getFullYear(),
          lastDay.getMonth(),
          lastDay.getDate() + 1
        ); // Set the countdown date to the next day

      count.forEach((item) => {
        const countDown = birthday.getTime(),
          x = setInterval(function () {
            const now = new Date().getTime(),
              distance = countDown - now;
            (item.querySelector(".days").innerText = Math.floor(
              distance / day
            )),
              (item.querySelector(".hours").innerText = Math.floor(
                (distance % day) / hour
              )),
              (item.querySelector(".minutes").innerText = Math.floor(
                (distance % hour) / minute
              )),
              (item.querySelector(".seconds").innerText = Math.floor(
                (distance % minute) / second
              ));
          }, 1000);
      });
    }

    var currentDate = new Date();

    // Set the date to the first day of the next month
    var firstDayOfNextMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1,
      1
    );
    // Subtract one day to get the last day of the current month
    var lastDayOfMonth = new Date(firstDayOfNextMonth.getTime() - 1);
    // Extract the day, month, and year from the last day of the current month
    var day = lastDayOfMonth.getDate();
    var month = lastDayOfMonth.getMonth() + 1;
    var year = lastDayOfMonth.getFullYear();

    var formattedDate =
      (day < 10 ? "0" : "") +
      day +
      "/" +
      (month < 10 ? "0" : "") +
      month +
      "/" +
      year;
    const setTimeRunning = document.querySelector(
      ".worthy .package-gif-count .time"
    );
    if (setTimeRunning) {
      setTimeRunning.innerText = formattedDate;
    }

    var retina = window.devicePixelRatio,
      // Math shorthands
      PI = Math.PI,
      sqrt = Math.sqrt,
      round = Math.round,
      random = Math.random,
      cos = Math.cos,
      sin = Math.sin,
      // Local WindowAnimationTiming interface
      rAF = window.requestAnimationFrame,
      cAF = window.cancelAnimationFrame || window.cancelRequestAnimationFrame,
      _now =
        Date.now ||
        function () {
          return new Date().getTime();
        };

    // Local WindowAnimationTiming interface polyfill
    (function (w) {
      var prev = _now();
      function fallback(fn) {
        var curr = _now();
        var ms = Math.max(0, 16 - (curr - prev));
        var req = setTimeout(fn, ms);
        prev = curr;
        return req;
      }

      /**
       * Cancel.
       */
      var cancel =
        w.cancelAnimationFrame ||
        w.webkitCancelAnimationFrame ||
        w.clearTimeout;

      rAF =
        w.requestAnimationFrame || w.webkitRequestAnimationFrame || fallback;

      cAF = function (id) {
        cancel.call(w, id);
      };
    })(window);

    document.addEventListener("DOMContentLoaded", function () {
      var speed = 50,
        duration = 1.0 / speed,
        confettiRibbonCount = 11,
        ribbonPaperCount = 30,
        ribbonPaperDist = 8.0,
        ribbonPaperThick = 8.0,
        confettiPaperCount = 95,
        DEG_TO_RAD = PI / 180,
        RAD_TO_DEG = 180 / PI,
        colors = [
          ["#df0049", "#660671"],
          ["#00e857", "#005291"],
          ["#2bebbc", "#05798a"],
          ["#ffd200", "#b06c00"],
        ];

      function Vector2(_x, _y) {
        (this.x = _x), (this.y = _y);
        this.Length = function () {
          return sqrt(this.SqrLength());
        };
        this.SqrLength = function () {
          return this.x * this.x + this.y * this.y;
        };
        this.Add = function (_vec) {
          this.x += _vec.x;
          this.y += _vec.y;
        };
        this.Sub = function (_vec) {
          this.x -= _vec.x;
          this.y -= _vec.y;
        };
        this.Div = function (_f) {
          this.x /= _f;
          this.y /= _f;
        };
        this.Mul = function (_f) {
          this.x *= _f;
          this.y *= _f;
        };
        this.Normalize = function () {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            this.x *= factor;
            this.y *= factor;
          }
        };
        this.Normalized = function () {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            return new Vector2(this.x * factor, this.y * factor);
          }
          return new Vector2(0, 0);
        };
      }
      Vector2.Lerp = function (_vec0, _vec1, _t) {
        return new Vector2(
          (_vec1.x - _vec0.x) * _t + _vec0.x,
          (_vec1.y - _vec0.y) * _t + _vec0.y
        );
      };
      Vector2.Distance = function (_vec0, _vec1) {
        return sqrt(Vector2.SqrDistance(_vec0, _vec1));
      };
      Vector2.SqrDistance = function (_vec0, _vec1) {
        var x = _vec0.x - _vec1.x;
        var y = _vec0.y - _vec1.y;
        return x * x + y * y + z * z;
      };
      Vector2.Scale = function (_vec0, _vec1) {
        return new Vector2(_vec0.x * _vec1.x, _vec0.y * _vec1.y);
      };
      Vector2.Min = function (_vec0, _vec1) {
        return new Vector2(
          Math.min(_vec0.x, _vec1.x),
          Math.min(_vec0.y, _vec1.y)
        );
      };
      Vector2.Max = function (_vec0, _vec1) {
        return new Vector2(
          Math.max(_vec0.x, _vec1.x),
          Math.max(_vec0.y, _vec1.y)
        );
      };
      Vector2.ClampMagnitude = function (_vec0, _len) {
        var vecNorm = _vec0.Normalized;
        return new Vector2(vecNorm.x * _len, vecNorm.y * _len);
      };
      Vector2.Sub = function (_vec0, _vec1) {
        return new Vector2(
          _vec0.x - _vec1.x,
          _vec0.y - _vec1.y,
          _vec0.z - _vec1.z
        );
      };

      function EulerMass(_x, _y, _mass, _drag) {
        this.position = new Vector2(_x, _y);
        this.mass = _mass;
        this.drag = _drag;
        this.force = new Vector2(0, 0);
        this.velocity = new Vector2(0, 0);
        this.AddForce = function (_f) {
          this.force.Add(_f);
        };
        this.Integrate = function (_dt) {
          var acc = this.CurrentForce(this.position);
          acc.Div(this.mass);
          var posDelta = new Vector2(this.velocity.x, this.velocity.y);
          posDelta.Mul(_dt);
          this.position.Add(posDelta);
          acc.Mul(_dt);
          this.velocity.Add(acc);
          this.force = new Vector2(0, 0);
        };
        this.CurrentForce = function (_pos, _vel) {
          var totalForce = new Vector2(this.force.x, this.force.y);
          var speed = this.velocity.Length();
          var dragVel = new Vector2(this.velocity.x, this.velocity.y);
          dragVel.Mul(this.drag * this.mass * speed);
          totalForce.Sub(dragVel);
          return totalForce;
        };
      }

      function ConfettiPaper(_x, _y) {
        this.pos = new Vector2(_x, _y);
        this.rotationSpeed = random() * 600 + 800;
        this.angle = DEG_TO_RAD * random() * 360;
        this.rotation = DEG_TO_RAD * random() * 360;
        this.cosA = 1.0;
        this.size = 5.0;
        this.oscillationSpeed = random() * 1.5 + 0.5;
        this.xSpeed = 40.0;
        this.ySpeed = random() * 60 + 50.0;
        this.corners = new Array();
        this.time = random();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        for (var i = 0; i < 4; i++) {
          var dx = cos(this.angle + DEG_TO_RAD * (i * 90 + 45));
          var dy = sin(this.angle + DEG_TO_RAD * (i * 90 + 45));
          this.corners[i] = new Vector2(dx, dy);
        }
        this.Update = function (_dt) {
          this.time += _dt;
          this.rotation += this.rotationSpeed * _dt;
          this.cosA = cos(DEG_TO_RAD * this.rotation);
          this.pos.x +=
            cos(this.time * this.oscillationSpeed) * this.xSpeed * _dt;
          this.pos.y += this.ySpeed * _dt;
          if (this.pos.y > ConfettiPaper.bounds.y) {
            this.pos.x = random() * ConfettiPaper.bounds.x;
            this.pos.y = 0;
          }
        };
        this.Draw = function (_g) {
          if (this.cosA > 0) {
            _g.fillStyle = this.frontColor;
          } else {
            _g.fillStyle = this.backColor;
          }
          _g.beginPath();
          _g.moveTo(
            (this.pos.x + this.corners[0].x * this.size) * retina,
            (this.pos.y + this.corners[0].y * this.size * this.cosA) * retina
          );
          for (var i = 1; i < 4; i++) {
            _g.lineTo(
              (this.pos.x + this.corners[i].x * this.size) * retina,
              (this.pos.y + this.corners[i].y * this.size * this.cosA) * retina
            );
          }
          _g.closePath();
          _g.fill();
        };
      }
      ConfettiPaper.bounds = new Vector2(0, 0);

      function ConfettiRibbon(
        _x,
        _y,
        _count,
        _dist,
        _thickness,
        _angle,
        _mass,
        _drag
      ) {
        this.particleDist = _dist;
        this.particleCount = _count;
        this.particleMass = _mass;
        this.particleDrag = _drag;
        this.particles = new Array();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        this.xOff = cos(DEG_TO_RAD * _angle) * _thickness;
        this.yOff = sin(DEG_TO_RAD * _angle) * _thickness;
        this.position = new Vector2(_x, _y);
        this.prevPosition = new Vector2(_x, _y);
        this.velocityInherit = random() * 2 + 4;
        this.time = random() * 100;
        this.oscillationSpeed = random() * 2 + 2;
        this.oscillationDistance = random() * 40 + 40;
        this.ySpeed = random() * 40 + 80;
        for (var i = 0; i < this.particleCount; i++) {
          this.particles[i] = new EulerMass(
            _x,
            _y - i * this.particleDist,
            this.particleMass,
            this.particleDrag
          );
        }
        this.Update = function (_dt) {
          var i = 0;
          this.time += _dt * this.oscillationSpeed;
          this.position.y += this.ySpeed * _dt;
          this.position.x += cos(this.time) * this.oscillationDistance * _dt;
          this.particles[0].position = this.position;
          var dX = this.prevPosition.x - this.position.x;
          var dY = this.prevPosition.y - this.position.y;
          var delta = sqrt(dX * dX + dY * dY);
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          for (i = 1; i < this.particleCount; i++) {
            var dirP = Vector2.Sub(
              this.particles[i - 1].position,
              this.particles[i].position
            );
            dirP.Normalize();
            dirP.Mul((delta / _dt) * this.velocityInherit);
            this.particles[i].AddForce(dirP);
          }
          for (i = 1; i < this.particleCount; i++) {
            this.particles[i].Integrate(_dt);
          }
          for (i = 1; i < this.particleCount; i++) {
            var rp2 = new Vector2(
              this.particles[i].position.x,
              this.particles[i].position.y
            );
            rp2.Sub(this.particles[i - 1].position);
            rp2.Normalize();
            rp2.Mul(this.particleDist);
            rp2.Add(this.particles[i - 1].position);
            this.particles[i].position = rp2;
          }
          if (
            this.position.y >
            ConfettiRibbon.bounds.y + this.particleDist * this.particleCount
          ) {
            this.Reset();
          }
        };
        this.Reset = function () {
          this.position.y = -random() * ConfettiRibbon.bounds.y;
          this.position.x = random() * ConfettiRibbon.bounds.x;
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          this.velocityInherit = random() * 2 + 4;
          this.time = random() * 100;
          this.oscillationSpeed = random() * 2.0 + 1.5;
          this.oscillationDistance = random() * 40 + 40;
          this.ySpeed = random() * 40 + 80;
          var ci = round(random() * (colors.length - 1));
          this.frontColor = colors[ci][0];
          this.backColor = colors[ci][1];
          this.particles = new Array();
          for (var i = 0; i < this.particleCount; i++) {
            this.particles[i] = new EulerMass(
              this.position.x,
              this.position.y - i * this.particleDist,
              this.particleMass,
              this.particleDrag
            );
          }
        };
        this.Draw = function (_g) {
          for (var i = 0; i < this.particleCount - 1; i++) {
            var p0 = new Vector2(
              this.particles[i].position.x + this.xOff,
              this.particles[i].position.y + this.yOff
            );
            var p1 = new Vector2(
              this.particles[i + 1].position.x + this.xOff,
              this.particles[i + 1].position.y + this.yOff
            );
            if (
              this.Side(
                this.particles[i].position.x,
                this.particles[i].position.y,
                this.particles[i + 1].position.x,
                this.particles[i + 1].position.y,
                p1.x,
                p1.y
              ) < 0
            ) {
              _g.fillStyle = this.frontColor;
              _g.strokeStyle = this.frontColor;
            } else {
              _g.fillStyle = this.backColor;
              _g.strokeStyle = this.backColor;
            }
            if (i == 0) {
              _g.beginPath();
              _g.moveTo(
                this.particles[i].position.x * retina,
                this.particles[i].position.y * retina
              );
              _g.lineTo(
                this.particles[i + 1].position.x * retina,
                this.particles[i + 1].position.y * retina
              );
              _g.lineTo(
                (this.particles[i + 1].position.x + p1.x) * 0.5 * retina,
                (this.particles[i + 1].position.y + p1.y) * 0.5 * retina
              );
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(
                (this.particles[i + 1].position.x + p1.x) * 0.5 * retina,
                (this.particles[i + 1].position.y + p1.y) * 0.5 * retina
              );
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else if (i == this.particleCount - 2) {
              _g.beginPath();
              _g.moveTo(
                this.particles[i].position.x * retina,
                this.particles[i].position.y * retina
              );
              _g.lineTo(
                this.particles[i + 1].position.x * retina,
                this.particles[i + 1].position.y * retina
              );
              _g.lineTo(
                (this.particles[i].position.x + p0.x) * 0.5 * retina,
                (this.particles[i].position.y + p0.y) * 0.5 * retina
              );
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(
                (this.particles[i].position.x + p0.x) * 0.5 * retina,
                (this.particles[i].position.y + p0.y) * 0.5 * retina
              );
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else {
              _g.beginPath();
              _g.moveTo(
                this.particles[i].position.x * retina,
                this.particles[i].position.y * retina
              );
              _g.lineTo(
                this.particles[i + 1].position.x * retina,
                this.particles[i + 1].position.y * retina
              );
              _g.lineTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            }
          }
        };
        this.Side = function (x1, y1, x2, y2, x3, y3) {
          return (x1 - x2) * (y3 - y2) - (y1 - y2) * (x3 - x2);
        };
      }
      ConfettiRibbon.bounds = new Vector2(0, 0);
      confetti = {};
      confetti.Context = function (id) {
        var i = 0;
        var canvas = document.getElementById(id);
        var canvasParent = canvas.parentNode;
        var canvasWidth = canvasParent.offsetWidth;
        var canvasHeight = canvasParent.offsetHeight;
        canvas.width = canvasWidth * retina;
        canvas.height = canvasHeight * retina;
        var context = canvas.getContext("2d");
        var interval = null;
        var confettiRibbons = new Array();
        ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiRibbonCount; i++) {
          confettiRibbons[i] = new ConfettiRibbon(
            random() * canvasWidth,
            -random() * canvasHeight * 2,
            ribbonPaperCount,
            ribbonPaperDist,
            ribbonPaperThick,
            45,
            1,
            0.05
          );
        }
        var confettiPapers = new Array();
        ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiPaperCount; i++) {
          confettiPapers[i] = new ConfettiPaper(
            random() * canvasWidth,
            random() * canvasHeight
          );
        }
        this.resize = function () {
          canvasWidth = canvasParent.offsetWidth;
          canvasHeight = canvasParent.offsetHeight;
          canvas.width = canvasWidth * retina;
          canvas.height = canvasHeight * retina;
          ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
          ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        };
        this.start = function () {
          this.stop();
          var context = this;
          this.update();
        };
        this.stop = function () {
          cAF(this.interval);
        };
        this.update = function () {
          var i = 0;
          context.clearRect(0, 0, canvas.width, canvas.height);
          for (i = 0; i < confettiPaperCount; i++) {
            confettiPapers[i].Update(duration);
            confettiPapers[i].Draw(context);
          }
          for (i = 0; i < confettiRibbonCount; i++) {
            confettiRibbons[i].Update(duration);
            confettiRibbons[i].Draw(context);
          }
          this.interval = rAF(function () {
            confetti.update();
          });
        };
      };
      var confetti = new confetti.Context("confetti");
      confetti.start();
      window.addEventListener("resize", function (event) {
        confetti.resize();
      });
    });
  }, []);
  return (
    <section className="module txt-white">
      <div className="module-member-wrap add-active-js">
        <div className="module-bg star">
          {" "}
          <img
            src="./assets/images/dich-vu-chup-profile/bg-star.png"
            alt="img-err"
          />
        </div>
        <div className="module-bg cloud-left">
          {" "}
          <img
            src="./assets/images/dich-vu-chup-profile/cloud-left.png"
            alt="img-err"
          />
        </div>
        <div className="module-bg cloud-right">
          {" "}
          <img
            src="./assets/images/dich-vu-chup-profile/cloud-right.png"
            alt="img-err"
          />
        </div>
        <div className="container">
          <div className="module-member">
            {" "}
            {/* <img
              className="image opti-image"
              data-src="./assets/hq-images/dich-vu-chup-profile/module-member.png"
              src="./assets/images/dich-vu-chup-profile/module-member.png"
              alt="img-err"
            /> */}
            <Image
              width={500}
              height={500}
              quality={100}
              className="image"
              src="/assets/hq-images/dich-vu-chup-profile/module-member.png"
              alt="img-err"
              priority
            />
            <div className="module-member-decor x1">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-01.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-01.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-01.png"
                alt="img-err"
              />
            </div>
            <div className="module-member-decor x2">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-02.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-02.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-02.png"
                alt="img-err"
              />
            </div>
            <div className="module-member-decor x3">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-03.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-03.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-03.png"
                alt="img-err"
              />
            </div>
            <div className="module-member-decor x4">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-04.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-04.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-04.png"
                alt="img-err"
              />
            </div>
            <div className="module-member-decor x5">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-05.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-05.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-05.png"
                alt="img-err"
              />
            </div>
            <div className="module-member-decor x6">
              {" "}
              {/* <img
                className="img opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-member-decor-06.png"
                src="./assets/images/dich-vu-chup-profile/module-member-decor-06.png"
                alt="img-err"
              /> */}
              <Image
                width={500}
                height={500}
                quality={100}
                className="img"
                src="/assets/hq-images/dich-vu-chup-profile/module-member-decor-06.png"
                alt="img-err"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="module-info">
        <div className="container">
          <div className="module-detail">
            <div className="module-detail-decor x1">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-01.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x2">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-02.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x3">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-03.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x4">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-04.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x5">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-05.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x6">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-06.png"
                alt="img-err"
              />
            </div>
            <div className="module-detail-decor x7">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/module-detail-decor-07.png"
                alt="img-err"
              />
            </div>
            <div className="module-header">
              <div className="module-header-tt fw-600">
                Gói chụp ảnh
                <br />
                <span className="fw-700 txt-Linear-gradient txt-upper">
                  SẢN PHẨM
                </span>
              </div>
              <div className="module-header-main">
                <div className="module-price fw-700">
                  <div className="txt">Chỉ từ</div>
                  <div className="price">
                    <img
                      src="./assets/images/chup-anh-doanh-nhan/price.png"
                      alt="img-err"
                    />
                  </div>
                </div>
                <div className="module-info-btn">
                  {" "}
                  <a
                    className="btn-four style-pri openPopMona"
                    href="https://mona.media/lien-he/"
                    data-popup="solutions"
                  >
                    <span className="icon">
                      {" "}
                      <img
                        src="./assets/images/chup-anh-doanh-nhan/phone.png"
                        alt="img-err"
                      />
                    </span>
                    <span className="txt">NHẬN TƯ VẤN TỪ ACCOUNT MONA</span>
                  </a>
                </div>
              </div>
            </div>
            <div className="module-detail-wrap">
              <div className="module-detail-item">
                <div className="module-detail-tt txt-center">
                  Giai đoạn
                  <br />
                  <span className="fw-700">TIỀN SẢN XUẤT</span>
                </div>
                <div className="module-detail-main">
                  <div className="module-detail-desc">
                    Các công việc tiền sản xuất nhằm
                    <span className="fw-700">
                      nắm rõ doanh nghiệp, hiểu rõ luận điểm bán hàng, điểm mạnh
                      và điểm yếu của doanh nghiệp
                    </span>
                  </div>
                  <div className="module-detail-inner">
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">
                        Tìm hiểu văn hóa, phong cách doanh nghiệp{" "}
                      </li>
                      <li className="module-detail-txt">
                        Thảo luận và đề xuất ý tưởng lên khách hàng
                      </li>
                    </ul>
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">
                        Chuẩn bị sẵn kịch bản và thứ tự chụp theo concept và yêu
                        cầu của khách hàng
                      </li>
                      <li className="module-detail-txt">
                        Plan storyboard cho phân đoạn chụp ảnh
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="module-detail-item">
                <div className="module-detail-tt txt-center">
                  Giai đoạn <br />
                  <span className="fw-700">SẢN XUẤT</span>
                </div>
                <div className="module-detail-main">
                  <div className="module-detail-inner">
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">1 Camera Man</li>
                      <li className="module-detail-txt">1 Assistant Camera</li>
                    </ul>
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">
                        <span className="txt-success fw-700">ĐÃ BAO GỒM</span>{" "}
                        tất cả các máy móc thiết bị kèm theo
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="module-detail-item">
                <div className="module-detail-tt txt-center">
                  Giai đoạn
                  <br />
                  <span className="fw-700">HẬU KỲ</span>
                </div>
                <div className="module-detail-main">
                  <div className="module-detail-desc">
                    Chuyên viên xử lý hình ảnh, hậu kỳ sẽ làm việc sau buổi chụp
                    tại<span className="fw-700">MONA STUDIO</span>
                  </div>
                  <div className="module-detail-inner">
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">
                        Lựa chọn, lọc hình ảnh
                      </li>
                      <li className="module-detail-txt">
                        Cắt và điều chỉnh khung hình
                      </li>
                      <li className="module-detail-txt">
                        Chỉnh sửa màu sắc và ánh sáng
                      </li>
                    </ul>
                    <ul className="module-detail-list">
                      <li className="module-detail-txt">
                        Loại bỏ các khuyết điểm nhỏ
                      </li>
                      <li className="module-detail-txt">
                        Retouching và làm đẹp
                      </li>
                      <li className="module-detail-txt">
                        Điều chỉnh độ phân giải
                      </li>
                    </ul>
                  </div>
                  <div className="module-detail-support">
                    <div className="module-detail-support-desc">
                      {" "}
                      <span className="fw-700">
                        Edit và Feedback 3 vòng{" "}
                      </span>{" "}
                      (Từ vòng 4, sẽ tính chi phí như giờ làm việc bình thường)
                    </div>
                    <ul className="module-detail-support-list">
                      <li className="module-detail-support-item">
                        7 ngày <span className="fw-700">sau khi onset</span>
                      </li>
                      <li className="module-detail-support-item">
                        Edit theo feedback vòng 1
                      </li>
                      <li className="module-detail-support-item">
                        Edit theo feedback vòng 2
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="module-info-add">
            <div className="module-info-add-main">
              <div className="module-info-add-bg">
                <img
                  src="./assets/images/dich-vu-chup-profile/module-info-add-bg.png"
                  alt="img-err"
                />
              </div>
              <div className="module-info-add-decor">
                <img
                  className="opti-image"
                  data-src="./assets/hq-images/dich-vu-chup-profile/module-info-add-decor.png"
                  src="./assets/images/dich-vu-chup-profile/module-info-add-decor.png"
                  alt="img-err"
                />
              </div>
              <div className="module-info-add-content">
                <div className="module-info-add-tt fw-700">
                  Ngoài ra, chúng tôi cung cấp
                  <span className="txt-host bonus-service">
                    các dịch vụ <br />
                    đi kèm
                  </span>
                </div>
                <ul className="module-info-add-list">
                  <li className="module-info-add-item">Studio di động</li>
                  <li className="module-info-add-item">Makeup chuyên nghiệp</li>
                  <li className="module-info-add-item">
                    Diễn viên (Đã bao gồm Makeup và phục trang){" "}
                  </li>
                </ul>
                <p className="module-info-add-desc">
                  * Trong trường hợp cần 1 số trang phục hoặc layout makeup đặc
                  biệt, sẽ có tư vấn chi tiết.
                </p>
              </div>
            </div>
          </div>
          <div className="module-arrow">
            <div className="arrow-down-wrapper">
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="module-handover">
            <div className="module-handover-decor x1">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-01.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-01.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-decor x2">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-02.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-02.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-decor x3">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-03.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-03.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-decor x4">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-04.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-04.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-decor x5">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-05.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-05.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-decor x6">
              <img
                className="cir opti-image"
                data-src="./assets/hq-images/dich-vu-chup-profile/module-handover-decor-06.png"
                src="./assets/images/dich-vu-chup-profile/module-handover-decor-06.png"
                alt="img-err"
              />
            </div>
            <div className="module-handover-tt fw-700 txt-center">
              <div className="module-handover-tt-inner">
                <div className="txt">Bàn giao dự án</div>
                <div className="txt">Bàn giao dự án</div>
                <div className="txt">Bàn giao dự án</div>
              </div>
            </div>
            <div className="module-handover-inner">
              <div className="module-handover-item">
                <div className="module-handover-icon">
                  {" "}
                  <img
                    src="./assets/images/chup-anh-doanh-nhan/module-handover-icon-01.png"
                    alt="img-err"
                  />
                </div>
                <div className="module-handover-txt">
                  {" "}
                  <span className="fw-700">Hình ảnh bản gốc</span> (RAW hoặc
                  JPG, PNG) theo hình đã lựa chọn
                </div>
              </div>
              <div className="module-handover-item">
                <div className="module-handover-icon">
                  {" "}
                  <img
                    src="./assets/images/chup-anh-doanh-nhan/module-handover-icon-02.png"
                    alt="img-err"
                  />
                </div>
                <div className="module-handover-txt">
                  {" "}
                  <span className="fw-700">Hình ảnh đã chỉnh sửa </span> (RAW
                  hoặc JPG, PNG) theo hình đã lựa chọn
                </div>
              </div>
              <div className="module-handover-item">
                <div className="module-handover-icon">
                  {" "}
                  <img
                    src="./assets/images/chup-anh-doanh-nhan/module-handover-icon-03.png"
                    alt="img-err"
                  />
                </div>
                <div className="module-handover-txt">
                  Các phiên bản thấp và cao phân giải của hình ảnh
                </div>
              </div>
              <div className="module-handover-item">
                <div className="module-handover-icon">
                  {" "}
                  <img
                    src="./assets/images/chup-anh-doanh-nhan/module-handover-icon-04.png"
                    alt="img-err"
                  />
                </div>
                <div className="module-handover-txt">Bản quyền hình ảnh</div>
              </div>
            </div>
            <div className="module-handover-special">
              <div className="module-handover-special-icon">
                {" "}
                <img
                  src="./assets/images/dich-vu-chup-profile/star.png"
                  alt="img-err"
                />
              </div>
              <div className="module-handover-special-txt">
                <span className="txt-nhtq-500 fw-700">ĐẶC BIỆT:</span> File
                hướng dẫn và tư vấn hình ảnh sau khi bàn giao
              </div>
            </div>
          </div>
          <div className="module-arrow">
            <div className="arrow-down-wrapper">
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
              <div className="arrow-group">
                <div className="arrow">
                  {" "}
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
                <div className="arrow">
                  <img
                    src="./assets/images/tkw-never-stop/arrow-down.svg"
                    alt="img-err"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="worthy txt-white">
          <div className="worthy-wrap">
            <div className="container">
              <div className="sec-com-tt fw-700 txt-center style-five">
                Mỗi tấm hình được chụp
                <br />
                XỨNG ĐÁNG SỐ TIỀN BẠN BỎ RA
              </div>
              <div className="worthy-tt txt-center">
                {" "}
                Với đội ngũ SÁNG TẠO, kỹ năng THƯỢNG THỪA <br />
                <span className="txt-host-500">
                  {" "}
                  MONA SẴN SÀNG LĂN XẢ ĐỂ CÓ BỘ ẢNH XÍ NGHIỆP, NHÀ MÁY XỊN XÒ
                  NHẤT
                </span>
              </div>
              <div className="worthy-list">
                <div className="worthy-item linear-edutech">
                  <div className="worthy-icon">
                    {" "}
                    <img
                      src="./assets/images/dich-vu-chup-profile/worthy-icon-01.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-text txt-center">
                    <span className="fw-700">Ekip năng động, </span> luôn lắng
                    nghe ý kiến khách hàng
                  </div>
                </div>
                <div className="worthy-item linear-host">
                  <div className="worthy-icon">
                    {" "}
                    <img
                      src="./assets/images/dich-vu-chup-profile/worthy-icon-02.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-text txt-center">
                    <span className="fw-700">Trang thiết bị hiện đại,</span>đa
                    năng
                  </div>
                </div>
                <div className="worthy-item linear-nhtq">
                  <div className="worthy-icon">
                    {" "}
                    <img
                      src="./assets/images/dich-vu-chup-profile/worthy-icon-03.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-text txt-center">
                    {" "}
                    Chụp hình thoải mái,
                    <span className="fw-700">vui vẻ như đang ở nhà</span>
                  </div>
                </div>
                <div className="worthy-item linear-media">
                  <div className="worthy-icon">
                    {" "}
                    <img
                      src="./assets/images/dich-vu-chup-profile/worthy-icon-04.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-text txt-center">
                    <span className="fw-700">Kịch bản sáng tạo,</span> thể hiện
                    đặc trưng doanh nghiệp
                  </div>
                </div>
                <div className="worthy-item linear-software">
                  <div className="worthy-icon">
                    {" "}
                    <img
                      src="./assets/images/dich-vu-chup-profile/worthy-icon-05.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-text txt-center">
                    <span className="fw-700">MONA Studio hoành tráng,</span> đa
                    dạng góc chụp
                  </div>
                </div>
              </div>
              <div className="worthy-gif add-active-js">
                <div className="worthy-gif-wrap">
                  <img
                    className="worthy-mask"
                    src="./assets/images/chup-anh-san-pham/worthy-mask.png"
                    alt="img-err"
                  />
                  <div className="bg-event">
                    <div className="inner">
                      {" "}
                      <img
                        className="bg-event-left opti-image"
                        data-src="./assets/hq-images/chup-anh-doanh-nhan/bg-event-left.png"
                        src="./assets/images/chup-anh-doanh-nhan/bg-event-left.png"
                        alt="img-err"
                      />
                      <img
                        className="bg-event-right opti-image"
                        data-src="./assets/hq-images/chup-anh-doanh-nhan/bg-event-right.png"
                        src="./assets/images/chup-anh-doanh-nhan/bg-event-right.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                  <div className="worthy-gif-tt">
                    {" "}
                    <img
                      className="img opti-image"
                      data-src="./assets/hq-images/chup-anh-san-pham/discount.png"
                      src="./assets/images/chup-anh-san-pham/discount.png"
                      alt="img-err"
                    />
                  </div>
                  <div className="worthy-gif-inner">
                    <div className="worthy-gif-item">
                      <div className="worthy-gif-inner-light">
                        <span id="label1" class="sr-only">
                          Description of the light image
                        </span>
                        <object
                          data="./assets/images/chup-anh-doanh-nhan/light.svg"
                          type="image/svg+xml"
                          aria-labelledby="label1"
                        ></object>
                      </div>
                      <div className="worthy-gif-image">
                        <div className="inner">
                          <img
                            className="img opti-image"
                            data-src="./assets/hq-images/chup-anh-san-pham/worthy-gif-image-01.png"
                            src="./assets/images/chup-anh-san-pham/worthy-gif-image-01.png"
                            alt="img-err"
                          />
                          <img
                            className="img opti-image"
                            data-src="./assets/hq-images/chup-anh-san-pham/worthy-gif-image-02.png"
                            src="./assets/images/chup-anh-san-pham/worthy-gif-image-02.png"
                            alt="img-err"
                          />
                        </div>
                        <div className="worthy-gif-txt fw-700">
                          <div className="txt fw-700">
                            Gói “Behind the scene”{" "}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="arrow-down-wrapper">
                    <div className="arrow-group">
                      <div className="arrow">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                      <div className="arrow">
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                    </div>
                    <div className="arrow-group">
                      <div className="arrow">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                      <div className="arrow">
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                    </div>
                    <div className="arrow-group">
                      <div className="arrow">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                      <div className="arrow">
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                    </div>
                    <div className="arrow-group">
                      <div className="arrow">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                      <div className="arrow">
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                    </div>
                    <div className="arrow-group">
                      <div className="arrow">
                        {" "}
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                      <div className="arrow">
                        <img
                          src="./assets/images/tkw-never-stop/arrow-down.svg"
                          alt="img-err"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="worthy-gif-add">
                    <div className="worthy-gif-add-item">
                      <div className="worthy-gif-add-tt fw-700">
                        Kèm thêm
                        <br />
                        <span className="txt-linear-host">
                          BỘ HƯỚNG DẪN SỬ DỤNG HÌNH ẢNH <br />
                          THÔNG MINH
                        </span>
                      </div>
                      <p className="worthy-gif-add-desc">
                        Đến từ chính chuyên gia Marketing
                      </p>
                      <div className="worthy-gif-add-decor">
                        {" "}
                        <img
                          src="./assets/images/dich-vu-chup-profile/worthy-gif-add-decor.png"
                          alt="img-err"
                        />
                      </div>
                    </div>
                    <div className="worthy-gif-add-item">
                      <div className="inner">
                        <div className="item">
                          {" "}
                          <span className="icon">
                            <img
                              src="./assets/images/dich-vu-chup-profile/worthy-gif-add.png"
                              alt="img-err"
                            />
                          </span>
                          <div className="txt">
                            Hướng dẫn cách tải và sử dụng ảnh
                          </div>
                        </div>
                        <div className="item">
                          {" "}
                          <span className="icon">
                            <img
                              src="./assets/images/dich-vu-chup-profile/worthy-gif-add.png"
                              alt="img-err"
                            />
                          </span>
                          <span className="txt">
                            Hướng dẫn cách chọn ảnh hiệu quả
                          </span>
                        </div>
                        <div className="item">
                          {" "}
                          <span className="icon">
                            <img
                              src="./assets/images/dich-vu-chup-profile/worthy-gif-add.png"
                              alt="img-err"
                            />
                          </span>
                          <span className="txt">
                            Dùng ảnh thế nào để tăng tương tác
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="worthy-gif-btn">
                    <span id="label2" class="sr-only">
                      Description of the light image
                    </span>
                    <object
                      className="decor"
                      data="./assets/images/dich-vu-chup-profile/circle.svg"
                      type="image/svg+xml"
                      aria-labelledby="label2"
                    ></object>
                    <span className="txt-wrap">
                      <span className="txt fw-700 txt-center">
                        Nhận ngay tổng giá trị quà tặng lên tới
                      </span>
                      <div className="img-money">
                        <img
                          src="./assets/images/chup-anh-san-pham/money.png"
                          alt="img-err"
                        />
                      </div>
                    </span>
                  </div>
                </div>
              </div>
              <div className="worthy-promotion">
                <canvas id="confetti"></canvas>
                <div className="worthy-promotion-price col-6 mg-auto">
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/chup-anh-san-pham/unique.png"
                    src="./assets/images/chup-anh-san-pham/unique.png"
                    alt="img-err"
                  />
                </div>
                <div className="worth-service-txt">
                  <span className="txt fw-700">
                    Khi đăng ký dịch vụ chụp ảnh sản phẩm
                  </span>
                  <br /> của chúng tôi tại page này!
                </div>
                <div className="gif-price-btn">
                  {" "}
                  <a
                    className="openPopMona grown-cta add-active-js"
                    href="https://mona.media/lien-he"
                    data-popup="soluions"
                  >
                    <div className="icon-thunder">
                      {" "}
                      <span className="bolt-ani bolt-ani-js">
                        <svg
                          className="bolt-ani-line left"
                          viewBox="0 0 170 57"
                        >
                          <path d="M36.2701759,17.9733192 C-0.981139498,45.4810755 -7.86361824,57.6618438 15.6227397,54.5156241 C50.8522766,49.7962945 201.109341,31.1461782 161.361488,2"></path>
                        </svg>
                        <svg
                          className="bolt-ani-line right"
                          viewBox="0 0 170 57"
                        >
                          <path d="M36.2701759,17.9733192 C-0.981139498,45.4810755 -7.86361824,57.6618438 15.6227397,54.5156241 C50.8522766,49.7962945 201.109341,31.1461782 161.361488,2"></path>
                        </svg>
                        <span className="ani">
                          <span></span>
                        </span>
                      </span>
                    </div>
                    <div className="grown-cta-txt txt-center">
                      <div className="title">Nhận ngay SIÊU ƯU ĐÃI</div>
                      <div className="desc">
                        Nhanh tay! MONA chỉ còn{" "}
                        <span className="fw-700">16 suất duy nhất</span>
                      </div>
                    </div>
                  </a>
                  <div className="image">
                    <img
                      src="./assets/images/skillhub/ani-button-02.svg"
                      alt="img-err"
                    />
                  </div>
                </div>
                <div className="package-gif-commit txt-center">
                  MONA cam kết tuyệt đối không sử dụng <br />
                  thông tin của bạn để bán hoặc SPAM
                  <div className="lock-decor">
                    <img
                      src="./assets/images/tkw-never-stop/key.png"
                      alt="img-err"
                    />
                  </div>
                </div>
                <div className="package-gif-count">
                  Chỉ áp dụng tới <span className="time"></span>
                </div>
                <div className="count-js style-second">
                  <ul id="countdown">
                    <li className="countdown-tiem">
                      <span className="days"></span>Ngày
                    </li>
                    <li className="countdown-tiem">
                      <span className="hours"></span>Giờ
                    </li>
                    <li className="countdown-tiem">
                      <span className="minutes"></span>Phút
                    </li>
                    <li className="countdown-tiem">
                      <span className="seconds"></span>Giây
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Script id="module">
        {`
            

        `}
      </Script>
    </section>
  );
}
