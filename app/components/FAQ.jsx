"use client";

export default function FAQ() {
  return (
    <section className="sec-seofaq">
      <div className="container">
        <div className="seofaq">
          <div className="seofaq-top">
            <div className="title text-ani bg-blue">
              FAQ
              <div className="seofaq-line">
                <svg
                  className="ani"
                  width="317"
                  height="165"
                  viewBox="0 0 317 165"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    className="svg-elem-1"
                    d="M239.914 54.4646C97.9425 -6.18229 6.55242 27.0745 2.17067 68.9596C-2.21107 110.845 64.1018 152.108 150.285 161.124C236.467 170.14 309.884 143.494 314.266 101.609C318.518 60.9678 222.966 13.0692 140.531 2.48972"
                    stroke="#7C0FD1"
                    strokeWidth="3.2557"
                  ></path>
                </svg>
              </div>
            </div>
          </div>
          <div className="seofaq-flex">
            <div className="seofaq-left">
              <div className="seofaq-title">
                <div className="title">Chúng tôi có thể giúp gì cho bạn?</div>
              </div>
              <div className="seofaq-bg-img">
                <div className="img">
                  <img
                    src="./assets/images/chup-anh-doanh-nhan/faq-img.png"
                    alt="img-err"
                  />
                </div>
              </div>
            </div>
            <div className="seofaq-right">
              <div className="seofaq-tt fw-700">
                Câu hỏi về dịch vụ Quay Chụp
              </div>
              <div className="seofaq-wrap collapse-block">
                <div className="seofaq-list">
                  <div className="seofaq-item collapse-item">
                    <div className="seofaq-head collapse-head">
                      <div className="title">
                        Cần thời gian bao lâu cho một gói chụp sản phẩm?
                      </div>
                      <img
                        className="fa-chevron-circle-down icon"
                        src="./assets/images/dvs/dropdown-icon.png"
                        alt="img-err"
                      />
                    </div>
                    <div className="seofaq-tbody collapse-body">
                      <ul className="seoif-des-list">
                        <li className="seoif-des-item">
                          Tùy theo gói chụp hình và số lượng ảnh mà thời gian
                          bàn giao sản phẩm sẽ khác nhau. Thông thường, sau 3 –
                          5 ngày làm việc khách hàng sẽ nhận được ảnh đã qua
                          chỉnh sửa hậu kỳ.
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="seofaq-item collapse-item">
                    <div className="seofaq-head collapse-head">
                      <div className="title">
                        MONA có giới hạn số lần chỉnh sửa hình ảnh chụp theo yêu
                        cầu không?
                      </div>
                      <img
                        className="fa-chevron-circle-down icon"
                        src="./assets/images/dvs/dropdown-icon.png"
                        alt="img-err"
                      />
                    </div>
                    <div className="seofaq-tbody collapse-body">
                      <ul className="seoif-des-list">
                        <li className="seoif-des-item">
                          MONA giới hạn số lần chỉnh sửa hình chụp tối đa là 2
                          lần. Bao gồm việc chỉnh sửa màu sắc, ánh sáng, nền
                          hoặc bất kỳ điều chỉnh nào khác theo yêu cầu của khách
                          hàng. Từ lần thứ 3 trở đi, MONA sẽ tính tiền chỉnh sửa
                          hình ảnh từ 150.000 – 250.000 VNĐ/hình.
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="seofaq-item collapse-item">
                    <div className="seofaq-head collapse-head">
                      <div className="title">
                        Các gói chụp hình sản phẩm tại MONA có giới hạn số lượng
                        không?
                      </div>
                      <img
                        className="fa-chevron-circle-down icon"
                        src="./assets/images/dvs/dropdown-icon.png"
                        alt="img-err"
                      />
                    </div>
                    <div className="seofaq-tbody collapse-body">
                      <ul className="seoif-des-list">
                        <li className="seoif-des-item">
                          Các gói chụp quay chụp hình chuyên nghiệp tại MONA
                          không có giới hạn về số lượng sản phẩm, chụp càng
                          nhiều giá càng ưu đãi. Bạn có thể chọn gói chụp hình
                          phù hợp với nhu cầu và ngân sách của mình.
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
