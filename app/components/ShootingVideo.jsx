"use client";

import React, { useEffect, useRef, useState } from "react";
import Script from "next/script";

export default function ShootingVideo() {
  const [showVideo, setShowVideo] = useState(false);
  const [isIntersecting, setIntersecting] = useState(false);
  const [isVisible, setIsVisible] = useState(false);

  const videoRef = useRef(null);

  const showVideoHandler = () => {
    setShowVideo(true);
  };

  useEffect(() => {
    const observer = new IntersectionObserver(([entry]) => {
      setIntersecting(entry.isIntersecting);
    });

    observer.observe(videoRef.current);

    // Disconnect the observer if isVisible becomes true
    if (isIntersecting) {
      setIsVisible(true);
      observer.disconnect();
    }

    return () => observer.disconnect();
  }, [videoRef, isIntersecting]);

  useEffect(() => {
    function isInViewport(element) {
      const rect = element.getBoundingClientRect();
      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <=
          (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <=
          (window.innerWidth || document.documentElement.clientWidth)
      );
    }

    function calculatePercentage(element) {
      const rect = element.getBoundingClientRect();

      const windowHeight =
        window.innerHeight || document.documentElement.clientHeight;
      if (rect && windowHeight) {
        const percentage =
          ((windowHeight - rect.bottom) / windowHeight) * 100 * 2;

        return Math.max(0, Math.min(100, percentage));
      }
    }

    function onScroll() {
      const scrollText = document.getElementById("concept-san-pham-js");
      let screenWidth = window.innerWidth;

      function updateVideoEffect() {
        let percentage = calculatePercentage(scrollText);
        const defaultWidthVideo = 15;

        if (percentage) {
          percentage = Math.min(percentage + defaultWidthVideo, 100);
          const scale = parseFloat((percentage.toFixed(1) / 100).toFixed(2));

          const transformValue =
            screenWidth && screenWidth > 768
              ? "scale(" + scale + ")"
              : "scale(1)";
          // Set transform directly using style property
          document.querySelector(".video-effect-js").style.transform =
            transformValue;
        }
      }

      if (scrollText && isInViewport(scrollText)) {
        updateVideoEffect();
      }
    }
    window.addEventListener("scroll", onScroll);
    document.addEventListener("DOMContentLoaded", onScroll);
    var videoblock = document.querySelectorAll(".video-block");
    if (videoblock.length > 0) {
      videoblock.forEach((item) => {
        var showVideo = item.querySelector(".btn-play-video-js");
        var btnPlayParent = item.querySelector(".btn-play");
        console.log(btnPlayParent);

        var btnPlayVideo = item.querySelector(".btn-play-video-js");
        showVideo.addEventListener("click", function () {
          var img = item.querySelector(".image-js");
          if (img) {
            img.style.transition =
              "opacity 0.5s ease-in-out, top 0.5s ease-in-out";
            img.style.top = "-100%";
            img.style.opacity = 0;
          }
          if (btnPlayVideo) {
            btnPlayVideo.style.transition =
              "opacity 0.5s ease-in-out, top 0.5s ease-in-out";
            btnPlayVideo.style.opacity = 0;
            btnPlayVideo.style.top = "100%";
            if (btnPlayParent) {
              btnPlayParent.style.zIndex = 0;
            }
          }
          var iframe = item.getElementsByTagName("iframe")[0].contentWindow;
          iframe.postMessage(
            '{"event":"command","func":"' + "playVideo" + '","args":""}',
            "*"
          );
        });
      });
    }

    return () => {
      window.removeEventListener("showVideo", showVideoHandler);
    };
  }, []);
  return (
    <section className="sec-com quay-video-san-pham">
      <img
        className="may-img"
        src="./assets/images/chup-anh-san-pham/may.png"
        alt="img-err"
      />
      <div className="container">
        <div className="banner-profile-wrap video-block">
          <div className="banner-profile-video">
            <div className="decor top-left">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/decor-01.png"
                alt="img-err"
              />
            </div>
            <div className="decor top-right">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/decor-02.png"
                alt="img-err"
              />
            </div>
            <div className="decor bottom-left">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/decor-03.png"
                alt="img-err"
              />
            </div>
            <div className="decor bottom-right">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/decor-04.png"
                alt="img-err"
              />
            </div>
            <div className="video-effect video-effect-js">
              <div className="video-title">
                4 phút để hiểu rõ hơn về{" "}
                <span className="fw-700"> CÁCH TẠO PROFILE CHUYÊN NGHIỆP</span>
                từ{" "}
                <img
                  className="img"
                  src="./assets/images/chup-anh-san-pham/the-mona.png"
                  alt="img-err"
                />
              </div>
              <div className="main-video" ref={videoRef}>
                <div className="img image-js">
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/chup-anh-doanh-nhan/photograph.jpg"
                    src="./assets/images/chup-anh-doanh-nhan/photograph.jpg"
                    alt="img-err"
                  />
                </div>
                {isVisible && (
                  <iframe
                    className="iframe-1 iframe-1-js"
                    src="https://www.youtube.com/embed/8UANGfJhaHE?rel=0&amp;enablejsapi=1"
                    allow="autoplay; encrypted-media"
                    frameBorder="0"
                    title="YouTube Video"
                  ></iframe>
                )}

                <div className="btn-play">
                  <div className="btn-play-video btn-play-video-js">
                    <img
                      src="./assets/images/chup-anh-doanh-nhan/btn-play-video.png"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
