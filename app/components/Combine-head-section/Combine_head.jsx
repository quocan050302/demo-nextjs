import React from "react";
import Banner from "../Banner";
import ShootingVideo from "../ShootingVideo";
import Factor from "../Factor";
import Combine from "../Combine/Combine";
import Slide from "../Slide";
import Marquee from "../Marquee";

export default function Combine_head() {
  return (
    <div className="combine-head-section">
      <img
        className="mask-01"
        src="./assets/images/chup-anh-san-pham/mask-01.png"
        alt="img-err"
      />
      <img
        className="mask-02"
        src="./assets/images/chup-anh-san-pham/mask-02.png"
        alt="img-err"
      />
      <img
        className="mask-03"
        src="./assets/images/chup-anh-san-pham/mask-03.png"
        alt="img-err"
      />
      <Banner />
      <ShootingVideo />
      <Factor />
      <Combine />
      <Slide />
      <Marquee />
    </div>
  );
}
