import Image from "next/image";
import Script from "next/script";
import React from "react";
const Banner = () => {
  return (
    <section className="sec-com banner-san-pham">
      <div>
        {/* <Image /> */}
        <img
          className="img-02 cir"
          src="./assets/images/chup-anh-san-pham/banner-sub-img-02.png"
          alt="img-err"
        />
        <div className="container">
          <div className="banner-container">
            <img
              className="img-01 cir"
              src="./assets/images/chup-anh-san-pham/banner-sub-img-01.png"
              alt="img-err"
            />
            <img
              className="img-03 cir"
              src="./assets/images/chup-anh-san-pham/banner-sub-img-03.png"
              alt="img-err"
            />
            <img
              className="img-04"
              src="./assets/images/chup-anh-san-pham/ly-giu-nhiet.png"
              alt="img-err"
            />
            <span className="txt-head">DỊCH VỤ CHỤP ẢNH SẢN PHẨM</span>
            <div className="img-top-banner">
              <img
                src="./assets/images/chup-anh-san-pham/banner-title.png"
                alt="img-err"
              />
            </div>
            <ul className="option-discount">
              <li className="discount-item">
                <img
                  className="img"
                  src="./assets/images/chup-anh-san-pham/check-green.png"
                  alt="img-err"
                />
                <div className="txt-disc">
                  <span>Giảm 10% </span> cho khách hàng đăng ký <br /> dịch vụ
                  lần đầu
                </div>
              </li>
              <li className="discount-item">
                <img
                  className="img"
                  src="./assets/images/chup-anh-san-pham/check-green.png"
                  alt="img-err"
                />
                <div className="txt-disc">
                  Tặng <span>01 video hậu trường </span> 15s
                </div>
              </li>
              <li className="discount-item">
                <img
                  className="img"
                  src="./assets/images/chup-anh-san-pham/check-green.png"
                  alt="img-err"
                />
                <div className="txt-disc">
                  <span>Bộ hướng dẫn </span> sử dụng thông minh
                </div>
              </li>
            </ul>
            <div className="module-info-btn">
              <a
                className="btn-four style-pri openPopMona"
                href="https://mona.media/lien-he/"
                data-popup="solutions"
              >
                <span className="icon">
                  <img
                    src="/assets/images/chup-anh-doanh-nhan/phone.png"
                    alt="img-err"
                  />
                </span>
                <span className="txt">NHẬN TƯ VẤN TỪ ACCOUNT MONA</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="sec-com concept-san-pham" id="concept-san-pham-js">
        <div className="container">
          <div className="concept-content">
            <div className="swiper swiper-concept swiper-concept-js gallery">
              <div className="swiper-wrapper">
                <div
                  className="swiper-slide gallery__img"
                  data-src="./assets/hq-images/chup-anh-san-pham/concept-01.png"
                >
                  <div className="img-wrapper">
                    <div className="img img-01">
                      <img
                        className="opti-image"
                        data-src="./assets/hq-images/chup-anh-san-pham/concept-01.png"
                        src="./assets/images/chup-anh-san-pham/concept-01.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="swiper-slide gallery__img"
                  data-src="./assets/hq-images/chup-anh-san-pham/concept-02.png"
                >
                  <div className="img-wrapper">
                    <div className="img img-02">
                      <img
                        className="opti-image"
                        data-src="./assets/hq-images/chup-anh-san-pham/concept-02.png"
                        src="./assets/images/chup-anh-san-pham/concept-02.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="swiper-slide gallery__img"
                  data-src="./assets/hq-images/chup-anh-san-pham/concept-03.png"
                >
                  <div className="img-wrapper">
                    <div className="img img-03">
                      <img
                        className="opti-image"
                        data-src="./assets/hq-images/chup-anh-san-pham/concept-03.png"
                        src="./assets/images/chup-anh-san-pham/concept-03.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="swiper-slide gallery__img"
                  data-src="./assets/hq-images/chup-anh-san-pham/concept-04.png"
                >
                  <div className="img-wrapper">
                    <div className="img img-04">
                      <img
                        className="opti-image"
                        data-src="./assets/hq-images/chup-anh-san-pham/concept-04.png"
                        src="./assets/images/chup-anh-san-pham/concept-04.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="swiper-slide gallery__img"
                  data-src="./assets/hq-images/chup-anh-san-pham/concept-05.png"
                >
                  <div className="img-wrapper">
                    <div className="img img-05">
                      <img
                        className="opti-image"
                        data-src="./assets/hq-images/chup-anh-san-pham/concept-05.png"
                        src="./assets/images/chup-anh-san-pham/concept-05.png"
                        alt="img-err"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="control-btn">
              <div className="swiper-button-prev"></div>
              <div className="swiper-button-next"></div>
            </div>
          </div>
        </div>
      </div>
      <Script id="show-banner">
        {`const cirs = document.querySelectorAll('.cir');
        var timer = Array;

        function randomXY(cir, x, y) {
            if (!x) {
                x = 10;
            }
            if (!y) {
                y = 15;
            }
            if (cir.classList.contains('cir-2')) {
                const translateX = Math.floor(Math.random() * x);
                const translateY = Math.floor(Math.random() * y);
                cir.style.transform = 'translate(' + translateX + '%' + ',' + translateY + '%' + ')';
            } else {
                const translateY = Math.floor(Math.random() * y);
                cir.style.transform = 'translate(' + 0 + '%' + ',' + -translateY + '%' + ')';
            }
        }

        for (let i = 0; i < cirs.length; i++) {
            let time = (i % 3) * 200 + 1500;
            const x = parseInt(cirs[i].getAttribute('data-x'));
            const y = parseInt(cirs[i].getAttribute('data-y'));
            timer[i] = setInterval(() => {
                randomXY(cirs[i], x, y);
            }, time);
        }
        var timer = Array;

        function randomXY(cir, x, y) {
            if (!x) {
                x = 10;
            }
            if (!y) {
                y = 15;
            }
            if (cir.classList.contains('cir-2')) {
                const translateX = Math.floor(Math.random() * x);
                const translateY = Math.floor(Math.random() * y);
                cir.style.transform = 'translate(' + translateX + '%' + ',' + translateY + '%' + ')';
            } else {
                const translateY = Math.floor(Math.random() * y);
                cir.style.transform = 'translate(' + 0 + '%' + ',' + -translateY + '%' + ')';
            }
        }

        for (let i = 0; i < cirs.length; i++) {
            let time = (i % 3) * 200 + 1500;
            const x = parseInt(cirs[i].getAttribute('data-x'));
            const y = parseInt(cirs[i].getAttribute('data-y'));
            timer[i] = setInterval(() => {
                randomXY(cirs[i], x, y);
            }, time);
        }`}
      </Script>
      <Script id="loadImages">
        {`
          function loadImages() {
            var lazyImages = document.querySelectorAll('.opti-image');
            lazyImages.forEach(function (img, index) {
                var imageSource = img.getAttribute('data-src');
                var imageSrcset = img.getAttribute('data-srcset');
                const imgload = [];
                const imgloadSrcset = [];
                
                if(imageSource) {
                    var newImage = new Image();
                    newImage.src = imageSource;
    
                    newImage.addEventListener('load', function () {
                        img.src = newImage.src;
                        img.removeAttribute('loading');
                    });
                }
                else if(imageSrcset) {
                    var newImage = new Image();
                    var srcsetLinks = imageSrcset.split(',').map(function(item) {
                        return item.trim().split(' ')[0];
                    }); 
                    imgload.push(...srcsetLinks);
                    imgloadSrcset.push(imageSrcset)
    
                    if (imgload.length >=2 ) {
                        if(window.innerWidth < 577) {
                            newImage.src = imgload[0]; 
                        }
                        else if (window.innerWidth < 992) {
                            newImage.src = imgload[1]; 
                        } else {
                            newImage.src = imgload[2];
                        }
                    }
    
                    // Thêm link từ data-src vào mảng imgload
                    newImage.addEventListener('load', function () {
                        img.src = newImage.src;
                        img.srcset = imgloadSrcset;
                        img.removeAttribute('data-srcset');
                        img.removeAttribute('loading');
                    })
    
                }
            });
        }
        loadImages();
          
        `}
      </Script>
    </section>
  );
};

export default Banner;
