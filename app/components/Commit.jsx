"use client";
import Image from "next/image";

export default function Commit() {
  return (
    <section className="commit">
      <div className="container">
        <div className="sec-com-tt fw-700 txt-center">
          Đảm bảo sản phẩm hiệu quả như cam kết
          <span className="txt-ani-bg radient-digital txt-border add-active-js txt-center active">
            <div className="txt">Quy trình dịch vụ chuyên môn hóa</div>
          </span>
        </div>
        <div className="commit-step">
          <div className="commit-step-item">
            <div className="commit-step-icon">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/check-01.png"
                alt="img-err"
              />
            </div>
            <div className="commit-step-txt fw-600">Thời gian rõ ràng</div>
          </div>
          <div className="commit-step-item">
            <div className="commit-step-icon">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/check-01.png"
                alt="img-err"
              />
            </div>
            <div className="commit-step-txt fw-600">Ekip chuyên nghiệp</div>
          </div>
          <div className="commit-step-item">
            <div className="commit-step-icon">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/check-01.png"
                alt="img-err"
              />
            </div>
            <div className="commit-step-txt fw-600">Tối ưu chi phí</div>
          </div>
        </div>
        <div className="commit-content">
          <div className="swiper swiper-commit">
            <div className="swiper-wrapper">
              <div className="swiper-slide">
                <div className="commit-item">
                  <div className="commit-item-content">
                    <div className="commit-tt fw-600 txt-media">
                      Giai đoạn 1: TIỀN SẢN XUẤT
                    </div>
                    <p className="commit-desc">
                      Để chúng tôi hiểu rõ về nhu cầu của khách hàng, về ngành,
                      phác thảo kịch bản video phù hợp nhất với doanh nghiệp
                    </p>
                    <ul className="commit-wrap">
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-01.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-lms fw-700">
                            Tiếp nhận yêu cầu
                          </span>{" "}
                          từ doanh nghiệp
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-02.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-software fw-700">
                            Nghiên cứu, tìm hiểu
                          </span>{" "}
                          văn hóa, quy mô, loại hình dịch vụ của doanh nghiệp{" "}
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-03.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          Nắm rõ các
                          <span className="txt-host fw-700">
                            “spotlight” và “keyword”
                          </span>{" "}
                          về bạn
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-04.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-nhtq fw-700">
                            Tư vấn giải pháp
                          </span>
                          , phương án phù hợp
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-05.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          Lên
                          <span className="txt-ecommerce fw-700">
                            moodboard, storyboard
                          </span>{" "}
                          (concept, trang phục, makeup, đạo cụ phù hợp với từng
                          ngành hàng,...)
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-06.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-digital fw-700">
                            Hoàn tất
                          </span>{" "}
                          khâu chuẩn bị
                        </span>
                      </li>
                    </ul>
                  </div>
                  <div className="commit-item-image">
                    {/* <img
                      className="img opti-image"
                      data-src="./assets/hq-images/quay-tvc/commit-item-image-01.jpg"
                      src="./assets/images/quay-tvc/commit-item-image-01.jpg"
                      alt="img-err"
                    /> */}
                    <Image
                      width={500}
                      height={500}
                      quality={100}
                      className="img"
                      src="/assets/hq-images/quay-tvc/commit-item-image-01.jpg"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
              <div className="swiper-slide">
                <div className="commit-item">
                  <div className="commit-item-content">
                    <div className="commit-tt fw-600 txt-media">
                      Giai đoạn 2: SẢN XUẤT
                    </div>
                    <p className="commit-desc">
                      Doanh nghiệp có thể lựa chọn địa điểm quay phù hợp với nhu
                      cầu của mình. Đội ngũ của MONA bao gồm account, media, hậu
                      cần đều theo sát bạn trong suốt quá trình on-set để cho ra
                      chất lượng source đúng với tinh thần doanh nghiệp nhất.{" "}
                    </p>
                    <ul className="commit-wrap style-pri">
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-07.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-lms fw-700">
                            Quay chụp tại địa điểm của doanh nghiệp:
                          </span>{" "}
                          Công ty, văn phòng, kho, xưởng,.
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-08.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-software fw-700">
                            Quay chụp tại Studio của MONA:
                          </span>
                          Toàn bộ thiết bị, nhân viên camera man, nhân viên hỗ
                          trợ, không tính thêm chi phí{" "}
                        </span>
                      </li>
                    </ul>
                  </div>
                  <div className="commit-item-image">
                    {/* <img
                      className="img opti-image"
                      data-src="./assets/hq-images/quay-tvc/commit-item-image-02.jpg"
                      src="./assets/images/quay-tvc/commit-item-image-02.jpg"
                      alt="img-err"
                    /> */}
                    <Image
                      width={500}
                      height={500}
                      quality={100}
                      className="img"
                      src="/assets/hq-images/quay-tvc/commit-item-image-02.jpg"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
              <div className="swiper-slide">
                <div className="commit-item">
                  <div className="commit-item-content">
                    <div className="commit-tt fw-600 txt-media">
                      Giai đoạn 3: HẬU KỲ
                    </div>
                    <ul className="commit-wrap style-second">
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-09.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          Quy trình edit và feedback tại MONA
                          <span className="txt-ecommerce fw-700">
                            thực hiện nghiêm ngặt theo timeline kế hoạch,
                          </span>{" "}
                          không kéo giãn thời gian làm phát sinh chi phí{" "}
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-10.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          Mọi tài nguyên MONA cung cấp cho bạn đảm bảo
                          <span className="txt-nhtq fw-700">
                            không bị đánh bản quyền trên mọi nền tảng
                          </span>{" "}
                          =&gt; Yên tâm lên chiến dịch marketing mà không sợ
                          giới hạn
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-11.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          Tiến hành{" "}
                          <span className="txt-lms fw-700">bàn giao</span> như
                          thỏa thuận trong hợp đồng
                        </span>
                      </li>
                      <li className="commit-text">
                        {" "}
                        <span className="icon">
                          {" "}
                          <img
                            src="./assets/images/dich-vu-chup-profile/commit-icon-12.png"
                            alt="img-err"
                          />
                        </span>
                        <span className="txt">
                          {" "}
                          <span className="txt-software fw-700">
                            Cung cấp file hướng dẫn và tư vấn sử dụng
                          </span>{" "}
                          sản phẩm sau khi bàn giao
                        </span>
                      </li>
                    </ul>
                  </div>
                  <div className="commit-item-image">
                    {/* <img
                      className="img opti-image"
                      data-src="./assets/hq-images/quay-tvc/commit-item-image-03.jpg"
                      src="./assets/images/quay-tvc/commit-item-image-03.jpg"
                      alt="img-err"
                    /> */}
                    <Image
                      width={500}
                      height={500}
                      quality={100}
                      className="img"
                      src="/assets/hq-images/quay-tvc/commit-item-image-03.jpg"
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="swiper-pagination"></div>
          <div className="swiper-button-next"></div>
          <div className="swiper-button-prev"></div>
        </div>
      </div>
    </section>
  );
}
