"use client";
import Image from "next/image";
import listImages from "../components/static/images/chup-anh-san-pham";

export default function Marquee() {
  return (
    <section className="marquee-san-pham">
      <div className="img-bg">
        <img
          className="opti-image"
          data-src="./assets/hq-images/chup-anh-san-pham/marquee-08.png"
          src="./assets/images/chup-anh-san-pham/marquee-08.png"
          alt="img-err"
        />
        {/* <Image
            // width={1920}
            // height={500}
            placeholder='blur'
            quality={100}
            src={listImages.marquee01}
            // className="img"
            // src="/assets/hq-images/chup-anh-san-pham/marquee-08.png"
            alt="img-err"
          /> */}
      </div>
      <div className="img-mask-02">
        <Image
          width={500}
          height={500}
          quality={100}
          src="/assets/images/chup-anh-san-pham/marquee-mask-02.png"
          alt="img-err"
        />
      </div>
      <div className="img-mask-03">
        <Image
          width={500}
          height={500}
          quality={100}
          src="/assets/images/chup-anh-san-pham/marquee-mask-03.png"
          alt="img-err"
        />
      </div>
      <div className="container">
        <div className="marquee-content">
          <div className="img-marquee-01">
            <img
              className="opti-image"
              src="./assets/images/chup-anh-san-pham/marquee-01.png"
              data-src="./assets/hq-images/chup-anh-san-pham/marquee-01.png"
              alt="img-err"
            />
            {/* <Image
              // width={500}
              // height={500}
              placeholder="blur"
              quality={100}
              src={listImages.marquee01}
              // className="img"
              // src="/assets/hq-images/chup-anh-san-pham/marquee-01.png"
              alt="img-err"
            /> */}
          </div>
          <div className="img-marquee-02 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee02}
              alt="img-err"
            />
          </div>
          <div className="img-marquee-03 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee03}
              alt="img-err"
            />
          </div>
          <div className="img-marquee-04 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee04}
              alt="img-err"
            />
          </div>
          <div className="img-marquee-05 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee05}
              alt="img-err"
            />
          </div>
          <div className="img-marquee-06 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee06}
              alt="img-err"
            />
          </div>
          <div className="img-marquee-07 img-marquee">
            <Image
              placeholder="blur"
              quality={100}
              src={listImages.marquee07}
              alt="img-err"
            />
          </div>
        </div>
      </div>
    </section>
  );
}
