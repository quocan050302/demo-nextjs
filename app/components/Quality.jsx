'use client'

export default function Quality() {
  return (
    <section className="quality">
      <div className="container">
        <div className="sec-com-tt txt-center">
          <span className="txt-ani-bg radient-digital add-active-js txt-center txt-border">
            <div className="txt">Đằng sau những thước phim ấn tượng</div>
          </span>
          Chúng tôi luôn nỗ lực để mang lại sản phẩm chất lượng nhất{" "}
        </div>
      </div>
      <div className="quality-content getWidth">
        <div className="quality-list gallery">
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-01.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-01.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-01.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-02.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-02.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-02.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-03.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-03.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-03.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-04.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-04.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-04.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-05.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-05.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-05.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-06.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-06.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-06.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-07.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-07.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-07.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-08.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-08.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-08.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-09.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-09.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-09.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-10.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-10.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-10.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-11.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-11.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-11.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-12.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-12.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-12.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-13.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-13.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-13.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-14.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-14.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-14.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-15.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-15.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-15.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-16.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-16.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-16.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-17.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-17.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-17.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-18.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-18.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-18.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-19.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-19.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-19.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-20.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-20.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-20.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-21.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-21.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-21.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-22.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-22.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-22.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-23.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-23.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-23.jpg"
              alt="img-err"
            />
          </div>
          <div
            className="quality-list-item gallery__img"
            data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-24.jpg"
          >
            {" "}
            <img
              className="img opti-image"
              data-src="./assets/hq-images/chup-anh-doanh-nhan/quality-item-24.jpg"
              src="./assets/images/chup-anh-doanh-nhan/quality-item-24.jpg"
              alt="img-err"
            />
          </div>
        </div>
      </div>
    </section>
  );
}
