import Image from "next/image";
import picture from "../../public/assets/hq-images/dich-vu-chup-profile/module-professional-img.png";

export default async function Module_Professional() {
  return (
    <section className="module-professional txt-white">
      <div className="module-professional-bg">
        {" "}
        <img
          src="./assets/images/chup-anh-doanh-nhan/mask-02.png"
          alt="img-err"
        />
      </div>
      <div className="module-professional-wrap">
        <div className="container">
          <div className="module-professional-tt txt-center">
            {" "}
            <span className="logo">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/logo.png"
                alt="img-err"
              />
            </span>
            giúp bạn sở hữu
            <br />
            <span className="txt-upper fw-700">
              <span className="txt-nhtq-400"> PROFILE CHUYÊN NGHIỆP</span> TĂNG
              CƠ HỘI HỢP TÁC
            </span>
            <br /> “Đầu tư cho bạn{" "}
            <span className="fw-700">- Đầu tư cho cơ hội kinh doanh”</span>
          </div>
          <div className="module-professional-content">
            <div className="sec-com-tt fw-700 txt-center">
              <span className="txt-ani-bg lms txt-border add-active-js txt-center active">
                <div className="txt">MONA đã có sẵn concept cho bạn!</div>
              </span>{" "}
              Chỉ cần liên hệ và trao đổi thông tin với chúng tôi
            </div>
            <p className="module-professinal-sub txt-center">
              Hỗ trợ Tư vấn 24/7 | Hoàn toàn miễn phí
            </p>
            <div className="module-professinal-btn">
              {" "}
              <a
                className="grown-cta add-active-js openPopMona"
                href="https://mona.media/lien-he/"
                data-popup="solutions"
              >
                <div className="icon-thunder">
                  {" "}
                  <span className="bolt-ani bolt-ani-js">
                    <svg className="bolt-ani-line left" viewBox="0 0 170 57">
                      <path d="M36.2701759,17.9733192 C-0.981139498,45.4810755 -7.86361824,57.6618438 15.6227397,54.5156241 C50.8522766,49.7962945 201.109341,31.1461782 161.361488,2"></path>
                    </svg>
                    <svg className="bolt-ani-line right" viewBox="0 0 170 57">
                      <path d="M36.2701759,17.9733192 C-0.981139498,45.4810755 -7.86361824,57.6618438 15.6227397,54.5156241 C50.8522766,49.7962945 201.109341,31.1461782 161.361488,2"></path>
                    </svg>
                    <span className="ani">
                      <span></span>
                    </span>
                  </span>
                </div>
                <div className="grown-cta-txt">
                  <div className="title">
                    Mọi thứ đã sẵn sàng cho một cuộc gọi
                  </div>
                  <div className="desc">
                    Khẩn trương! Lượt nhấn nút có giới hạn để giúp bạn có một
                    dịch vụ chụp hình tốt nhất
                  </div>
                </div>
              </a>
            </div>
            <div className="commit-spam txt-center">
              MONA cam kết tuyệt đối không sử dụng
              <br />
              thông tin của bạn để bán hoặc SPAM
              <div className="icon-key">
                <img
                  src="./assets/images/tkw-never-stop/key.png"
                  alt="img-err"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="module-professional-img">
          {" "}
          <Image
            width={0}
            height={0}
            sizes="100vw"
            quality={100}
            // placeholder="blur"
            priority
            src={picture}
            alt="img-err"
            style={{
              maxWidth: "100%",
              height: "auto",
            }}
          />
        </div>
      </div>
    </section>
  );
}
