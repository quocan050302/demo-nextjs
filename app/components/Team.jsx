"use client";

export default function Team() {
  return (
    <section className="team txt-white">
      <div className="team-wrap">
        <div className="team-wrap-bg">
          <div className="inner"></div>
        </div>
        <div className="container">
          <div className="sec-com-tt txt-center">
            Đội ngũ Ekip
            <span className="txt-digital">chuyên nghiệp, đầy tâm huyết</span>
            <br />
            <span className="fw-700">sẵn sàng cho các ý tưởng</span>
            <span className="txt-ani-bg media txt-border add-active-js txt-center active">
              <div className="txt">Sáng tạo</div>
            </span>
          </div>
          <div className="team-list add-active-js">
            <div className="team-item">
              <div className="team-item-inner add-active-js">
                <div className="team-item-bg">
                  <div className="inner">
                    <img
                      className="opti-image"
                      data-src="./assets/hq-images/chup-anh-doanh-nhan/team-tag-01.png"
                      src="./assets/images/chup-anh-doanh-nhan/team-tag-01.png"
                      alt="person-err"
                    />
                    <div className="team-item-decor-text">
                      <div className="animate-text">
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item style-pri">
                          {" "}
                          <span className="txt-software">EDIT</span>OR
                        </span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                        <span className="animate-text-item">EDITOR</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="team-item-member">
                  {" "}
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/dich-vu-chup-profile/team-member-01.png"
                    src="./assets/images/dich-vu-chup-profile/team-member-01.png"
                    alt="person-err"
                  />
                </div>
                <div className="team-item-decor fw-700 txt-upper">
                  <div className="jt">
                    <span className="jt__row">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-software">EDIT</span>OR
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="team-item">
              <div className="team-item-inner add-active-js">
                <div className="team-item-bg">
                  <div className="inner">
                    <img
                      className="opti-image"
                      data-src="./assets/hq-images/chup-anh-doanh-nhan/team-tag-02.png"
                      src="./assets/images/chup-anh-doanh-nhan/team-tag-02.png"
                      alt="person-err"
                    />
                    <div className="team-item-decor-text style-pri">
                      <div className="animate-text">
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item style-pri">
                          STY<span className="txt-ecommerce">LIST</span>
                        </span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                        <span className="animate-text-item">STYLIST</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="team-item-member">
                  {" "}
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/dich-vu-chup-profile/team-member-02.png"
                    src="./assets/images/dich-vu-chup-profile/team-member-02.png"
                    alt="img-err"
                  />
                </div>
                <div className="team-item-decor fw-700 txt-upper">
                  <div className="jt">
                    <span className="jt__row">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">STY</span>LIST
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="team-item">
              <div className="team-item-inner add-active-js">
                <div className="team-item-bg">
                  <div className="inner">
                    <img
                      className="opti-image"
                      data-src="./assets/hq-images/chup-anh-doanh-nhan/team-tag-03.png"
                      src="./assets/images/chup-anh-doanh-nhan/team-tag-03.png"
                      alt="person-err"
                    />
                    <div className="bg-text-container-wrap">
                      <div className="animate-text-js">
                        <span className="animate-text-re">
                          CREATIVE DIRECTOR 
                        </span>
                        <span className="animate-text-re">
                          CREATIVE DIRECTOR 
                        </span>
                      </div>
                      <div className="animate-text-js left">
                        <span className="animate-text-re">
                          CREATIVE DIRECTOR 
                        </span>
                        <span className="animate-text-re">
                          CREATIVE DIRECTOR 
                        </span>
                      </div>
                    </div>
                    <div className="bg-text-container-main">
                      CREATIVE
                      <br />
                      DIRECTOR{" "}
                    </div>
                  </div>
                </div>
                <div className="team-item-member">
                  {" "}
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/dich-vu-chup-profile/team-member-03.png"
                    src="./assets/images/dich-vu-chup-profile/team-member-03.png"
                    alt="img-err"
                  />
                </div>
                <div className="team-item-decor fw-700 txt-upper txt-digital">
                  <div className="jt">
                    <span className="jt__row">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        CREATIVE
                        <br />
                        DIRECTOR
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="team-item">
              <div className="team-item-inner add-active-js">
                <div className="team-item-bg">
                  <div className="inner">
                    <img
                      className="opti-image"
                      data-src="./assets/hq-images/chup-anh-doanh-nhan/team-tag-04.png"
                      src="./assets/images/chup-anh-doanh-nhan/team-tag-04.png"
                      alt="person-err"
                    />
                    <div className="team-item-decor-text style-three">
                      <div className="animate-text">
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item style-pri">
                          {" "}
                          <span className="txt-nhtq">CONTENT</span>
                        </span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                        <span className="animate-text-item">CONTENT</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="team-item-member">
                  {" "}
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/dich-vu-chup-profile/team-member-04.png"
                    src="./assets/images/dich-vu-chup-profile/team-member-04.png"
                    alt="img-err"
                  />
                </div>
                <div className="team-item-decor fw-700 txt-upper">
                  <div className="jt">
                    <span className="jt__row">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-nhtq">CON</span>TENT
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="team-item">
              <div className="team-item-inner add-active-js">
                <div className="team-item-bg">
                  <div className="inner">
                    <img
                      className="opti-image"
                      data-src="./assets/hq-images/chup-anh-doanh-nhan/team-tag-05.png"
                      src="./assets/images/chup-anh-doanh-nhan/team-tag-05.png"
                      alt="person-err"
                    />
                    <div className="team-item-decor-text style-second">
                      <div className="animate-text">
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item style-pri">
                          {" "}
                          <span className="txt-edutech-500">PHOTO</span>GRAPHER
                        </span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                        <span className="animate-text-item">PHOTOGRAPHER</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="team-item-member">
                  {" "}
                  <img
                    className="opti-image"
                    data-src="./assets/hq-images/dich-vu-chup-profile/team-member-05.png"
                    src="./assets/images/dich-vu-chup-profile/team-member-05.png"
                    alt="img-err"
                  />
                </div>
                <div className="team-item-decor fw-700 txt-upper">
                  <div className="jt">
                    <span className="jt__row">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                    <span className="jt__row jt__row--sibling">
                      <span className="jt__text">
                        <span className="txt-ecommerce">PHOTO</span>GRAPHER
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="team-mess">
            <div className="team-mess-decor">
              {" "}
              <img
                src="./assets/images/dich-vu-chup-profile/team-mess-decor.png"
                alt="error"
              />
            </div>
            <div className="team-mess-desc txt-center">
              Để đạt được tiêu chuẩn sản phẩm đã đề ra, chúng tôi luôn đề cao
              quy trình làm việc
              <br />
              thống nhất, hợp tác chặt chẽ giữa các bên từ Account, Creative
              Director,
              <br />
              Photographer, Producer, Editor,... và nhu cầu từ phía khách hàng.
            </div>
            <div className="team-mess-sub">
              <div className="txt txt-center fw-700">
                Từ đó, đảm bảo khắc họa đúng tinh thần thương hiệu, màu sắc của{" "}
                <br />
                doanh nghiệp một cách ấn tượng nhất, phô bày màu sắc doanh
                nghiệp
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
