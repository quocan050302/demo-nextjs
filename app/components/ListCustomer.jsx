"use client";

import React, { useEffect } from "react";
import Script from "next/script";

export default function ListCustomer() {
  useEffect(() => {
    const animation = document.querySelectorAll(".co-customer-item");
    if (animation) {
      animation.forEach((el) => {
        el.addEventListener("mouseenter", function checkHover() {
          el.classList.remove("animated-slide-out");
          el.classList.remove("mask-slide-out");

          el.classList?.add("animated-js");
        });
        el.addEventListener("mouseleave", function checkHover() {
          const maskInside = el.querySelector(".co-customer-wrap-mask");

          setTimeout(() => {
            el.classList.remove("animated-js");
            el.classList?.add("animated-slide-out");
            maskInside.classList?.add("mask-slide-out");
          }, 2000);
          setTimeout(() => {
            el.classList?.add("trans");
            el.classList.remove("animated-slide-out");
            maskInside.classList?.add("mask-transition");
            maskInside.classList.remove("mask-slide-out");
          }, 2300);
          setTimeout(() => {
            el.classList.remove("trans");
            maskInside.classList.remove("mask-transition");
          }, 2600);
        });
      });
    }
  }, []);
  return (
    <section className="sec-com list-customer txt-center">
      <div className="list-customer-img">
        {" "}
        <img
          src="./assets/images/chup-anh-san-pham/marquee-mask.png"
          alt="img-err"
        />
      </div>
      <div className="sec-com-header list-customer-header" data-aos="fade-up">
        <div className="container">
          <div className="sec-com-tt list-customer-tt">
            <h2>
              <p></p>Bạn đã sẵn sàng
              <span className="txt-bg txt-bg-ani bg-lms add-active-js mg-auto">
                <span className="txt"> Đồng hành cùng chúng tôi?</span>
              </span>
            </h2>
          </div>
          <div className="txt-desc">
            Trôi qua <span className="txt-digital fw-700"> hơn 8+ năm </span>
            phát triển. Chúng tôi đã đồng hành <br /> với
            <span className="txt-lms fw-700"> hơn 12.000+ khách hàng </span> từ
            Á đến Âu.
          </div>
          <div className="customer-logo-grid">
            <div className="co-customer-list d-flex">
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc1.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc1.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc2.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc2.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc3.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc3.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc4.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc4.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc5.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc5.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc6.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc6.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc7.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc7.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc8.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc8.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc9.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc9.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc10.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc10.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc11.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc11.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc12.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc12.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc13.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc13.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc14.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc14.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc15.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc15.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc16.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc16.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc17.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc17.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc18.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc18.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc19.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc19.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc20.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc20.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc21.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc21.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc22.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc22.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc23.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc23.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc24.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc24.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc25.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc25.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc26.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc26.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc27.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc27.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc28.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc28.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc29.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc29.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc30.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc30.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc31.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc31.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc32.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc32.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc33.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc33.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc34.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc34.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc35.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc35.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc36.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc36.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc37.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc37.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc38.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc38.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc39.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc39.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc40.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc40.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc41.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc41.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc42.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc42.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc43.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc43.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc44.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc44.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc45.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc45.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc46.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc46.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc47.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc47.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc48.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc48.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc49.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc49.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc50.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc50.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc51.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc51.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc52.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc52.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc53.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc53.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc54.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc54.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc55.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc55.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc56.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc56.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc57.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc57.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc58.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc58.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc59.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc59.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc60.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc60.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc61.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc61.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc62.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc62.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc63.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc63.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc64.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc64.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc65.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc65.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc66.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc66.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc67.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc67.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc68.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc68.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc69.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc69.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc70.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc70.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc71.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc71.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc72.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc72.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc73.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc73.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc74.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc74.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc75.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc75.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc76.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc76.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc77.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc77.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc78.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc78.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc79.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc79.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc80.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc80.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc81.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc81.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc82.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc82.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc83.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc83.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc84.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc84.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc85.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc85.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc86.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc86.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc87.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc87.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc88.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc88.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc89.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc89.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc90.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc90.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc91.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc91.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc92.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc92.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc93.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc93.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc94.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc94.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc95.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc95.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc96.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc96.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc97.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc97.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc98.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc98.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc99.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/customer/lc99.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc100.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc100.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc101.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc101.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc102.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc102.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc103.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc103.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc104.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc104.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc105.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc105.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc106.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc106.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc107.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc107.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc108.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc108.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc109.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc109.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc110.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc110.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc111.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc111.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc112.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc112.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc113.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc113.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc114.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc114.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc115.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc115.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem width-2">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc116.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc116.png)`,
                  }}
                ></div>
              </div>
              <div className="co-customer-item isotopeItem">
                <div className="co-customer-wrap item">
                  <img
                    src="./assets/images/chup-anh-san-pham/list-customer/lc117.png"
                    alt="img-err"
                  />
                </div>
                <div
                  className="co-customer-wrap-mask"
                  style={{
                    "--index": `url(/assets/images/chup-anh-san-pham/list-customer/lc117.png)`,
                  }}
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <Script id="card-hover">
        {`
          
        `}
      </Script>
       */}
    </section>
  );
}
