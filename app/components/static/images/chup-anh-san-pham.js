
// Purpose Section
import purposeSlide01 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-01.jpg";
import purposeSlide02 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-02.png";
import purposeSlide03 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-03.png";
import purposeSlide04 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-04.jpg";
import purposeSlide05 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-05.png";
import purposeSlide06 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-06.png";
import purposeSlide07 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-07.jpg";
import purposeSlide08 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-08.jpg";
import purposeSlide09 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-09.jpg";
import purposeSlide10 from "../../../../public/assets/hq-images/chup-anh-san-pham/purpose-slide-10.png";

// Service Section
import service03 from "../../../../public/assets/hq-images/chup-anh-san-pham/service-03.png";
import service04 from "../../../../public/assets/hq-images/chup-anh-san-pham/service-04.png";
import service05 from "../../../../public/assets/hq-images/chup-anh-san-pham/service-05.png";
import service06 from "../../../../public/assets/hq-images/chup-anh-san-pham/service-06.png";

// Slide Section
import slideImg01 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-1.jpg";
import slideImg02 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-2.jpg";
import slideImg03 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-3.jpg";
import slideImg04 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-4.jpg";
import slideImg05 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-5.jpg";
import slideImg06 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-6.jpg";
import slideImg07 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-7.jpg";
import slideImg08 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-8.jpg";
import slideImg09 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-9.jpg";
import slideImg10 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-10.jpg";
import slideImg11 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-11.jpg";
import slideImg12 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-12.jpg";
import slideImg13 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-13.jpg";
import slideImg14 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-14.jpg";
import slideImg15 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-15.jpg";
import slideImg16 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-16.jpg";
import slideImg17 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-17.jpg";
import slideImg18 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-18.jpg";
import slideImg19 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-19.jpg";
import slideImg20 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-20.jpg";
import slideImg21 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-21.jpg";
import slideImg22 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-22.jpg";
import slideImg23 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-23.jpg";
import slideImg24 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-24.jpg";
import slideImg25 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-25.jpg";
import slideImg26 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-26.jpg";
import slideImg27 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-27.jpg";
import slideImg28 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-28.jpg";
import slideImg29 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-29.jpg";
import slideImg30 from "../../../../public/assets/hq-images/chup-anh-xi-nghiep/slide-img-30.jpg";

// Marquee Section
import marquee01 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-01.png";
import marquee02 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-02.png";
import marquee03 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-03.png";
import marquee04 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-04.png";
import marquee05 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-05.png";
import marquee06 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-06.png";
import marquee07 from "../../../../public/assets/hq-images/chup-anh-san-pham/marquee-07.png";



const images = {
    purposeSlide01,
    purposeSlide02,
    purposeSlide03,
    purposeSlide04,
    purposeSlide05,
    purposeSlide06,
    purposeSlide07,
    purposeSlide08,
    purposeSlide09,
    purposeSlide10,
    service03,
    service04,
    service05,
    service06,
    slideImg01,
    slideImg02,
    slideImg03,
    slideImg04,
    slideImg05,
    slideImg06,
    slideImg07,
    slideImg08,
    slideImg09,
    slideImg10,
    slideImg11,
    slideImg12,
    slideImg13,
    slideImg14,
    slideImg15,
    slideImg16,
    slideImg17,
    slideImg18,
    slideImg19,
    slideImg20,
    slideImg21,
    slideImg22,
    slideImg23,
    slideImg24,
    slideImg25,
    slideImg26,
    slideImg27,
    slideImg28,
    slideImg29,
    slideImg30,
    marquee01,
    marquee02,
    marquee03,
    marquee04,
    marquee05,
    marquee06,
    marquee07
};

export default images;