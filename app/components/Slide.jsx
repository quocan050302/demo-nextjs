"use client";
import Image from "next/image";
import listImages from "../components/static/images/chup-anh-san-pham";

export default function Slide() {
  return (
    <section className="sec-com slide">
      <div className="container">
        <h2 className="txt-center txt-title">
          <span className="txt-bg txt-bg-ani add-active-js mg-auto">
            <div className="txt">Sự chân thực, tính thẩm mỹ từ hình ảnh</div>
          </span>
          <div className="txt-sub-head">
            Giúp bạn <span className="txt-color"> xây dựng được lòng tin </span>{" "}
            với khách hàng của bạn{" "}
          </div>
        </h2>
        <div className="txt-desc">
          Cùng xem <span className="txt-nhtq"> những albums chất lượng </span>{" "}
          từ khách hàng của MONA
        </div>
      </div>
      <div className="slide-images-container gallery">
        <div className="slide-bottom-bg">
          {" "}
          <Image
            width={500}
            height={500}
            className=""
            src="/assets/hq-images/chup-anh-xi-nghiep/pride-top-bg.png"
            alt="img-err"
            quality={100}
          />
          {/* <img
            className="opti-image"
            data-srcset="./assets/hq-images/chup-anh-xi-nghiep/pride-top-bg-sm.png 576w, ./assets/hq-images/chup-anh-xi-nghiep/pride-top-bg-md.png 960w, ./assets/hq-images/chup-anh-xi-nghiep/pride-top-bg.png 961w"
            srcSet="./assets/images/chup-anh-xi-nghiep/pride-top-bg-sm.png 576w, ./assets/images/chup-anh-xi-nghiep/pride-top-bg-md.png 960w, ./assets/images/chup-anh-xi-nghiep/pride-top-bg.png 961w"
            src="./assets/images/chup-anh-xi-nghiep/pride-top-bg.png"
            sizes="(max-width: 576px) 576px, (max-width: 991px) 991px, 1000px"
            alt="img-err"
            loading="lazy"
          /> */}
        </div>
        <div className="slide-images-list">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-1.jpg"
          >
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg01}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-1.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-2.jpg"
          >
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg02}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-2.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-3.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg03}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-3.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-4.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg04}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-4.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-5.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg05}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-5.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
        <div className="slide-images-list slide-revert">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-6.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg06}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-6.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-7.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg07}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-7.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-8.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg08}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-8.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-9.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg09}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-9.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-10.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg10}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-10.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
        <div className="slide-images-list">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-11.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg11}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-11.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-12.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg12}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-12.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-13.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg13}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-13.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-14.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg14}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-14.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-15.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg15}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-15.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
        <div className="slide-images-list slide-revert">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-16.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg16}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-16.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-17.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg17}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-17.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-18.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg18}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-18.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-19.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg19}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-19.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-20.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg20}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-20.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
        <div className="slide-images-list">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-21.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg21}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-21.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-22.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg22}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-22.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-23.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg23}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-23.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-24.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg24}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-24.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-25.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg25}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-25.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
        <div className="slide-images-list slide-revert">
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-26.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg26}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-26.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-27.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg27}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-27.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-28.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg28}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-28.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-29.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg29}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-29.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
          <div
            className="slide-image-item gallery__img"
            data-src="./assets/hq-images/chup-anh-xi-nghiep/slide-img-popup-30.jpg"
          >
            {" "}
            <Image
              placeholder="blur"
              // width={500}
              // height={500}
              className=""
              src={listImages.slideImg30}
              // src="/assets/hq-images/chup-anh-xi-nghiep/slide-img-30.jpg"
              alt="img-err"
              quality={100}
            />
          </div>
        </div>
      </div>
    </section>
  );
}
