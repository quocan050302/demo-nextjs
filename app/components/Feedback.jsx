"use client";
import Image from "next/image";

export default function Feedback() {
  return (
    <section className="sec-com feedback txt-center">
      <div className="container">
        <div className="sec-com-tt txt-center">
          <span className="txt-ani-bg radient-digital add-active-js txt-center txt-border">
            <div className="txt">Sự tận tâm, nhiệt tình của chúng tôi </div>
          </span>
          đã được công nhận bởi rất nhiều khách hàng
        </div>
        <div className="feedback-content gallery" id="gallery">
          <div className="all-top-other">
            <div className="all-top-other-content add-active-js">
              <div className="container">
                <div className="list d-flex f-ctn">
                  <div className="item">
                    <div className="item-wrap">
                      <div className="item-wrap-i">
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-1.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-2.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-3.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-3.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-3.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-4.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-5.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div className="item-wrap-i">
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-1.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-1.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-2.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-2.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-3.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-3.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-3.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-3.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-4.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-4.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-5.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-5.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="item-wrap">
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-6.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-7.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-8.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-9.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-10.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-6.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-6.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-7.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-7.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-8.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-8.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-9.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-9.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-10.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-10.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="item-wrap">
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-11.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-12.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-13.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-14.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-15.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-11.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-11.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-12.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-12.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-13.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-13.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-14.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-14.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-15.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-15.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="item-wrap">
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-16.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-17.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-18.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-19.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-20.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-16.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-16.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-17.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-17.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-18.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-18.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-19.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-19.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-20.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-20.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="item-wrap">
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-21.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-22.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-23.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-24.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-25.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                      <div className="item-wrap-i">
                        <div
                          className="img gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-21.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-21.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-22.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-22.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-23.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-23.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                        >
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-24.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-24.jpg"
                            alt="img-err"
                          />
                        </div>
                        <div
                          className="img trigger-gallery-js gallery__img"
                          data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                        >
                          {" "}
                          {/* <img
                            className="opti-image"
                            data-src="./assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                            src="./assets/images/dich-vu-chup-profile/all-top-other-25.jpg"
                            alt="img-err"
                          /> */}
                          <Image
                            width={500}
                            height={500}
                            quality={100}
                            // className="img"
                            src="/assets/hq-images/dich-vu-chup-profile/all-top-other-25.jpg"
                            alt="img-err"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
