"use client";
import React from "react";
import Image from "next/image";
import listImages from "../../components/static/images/chup-anh-san-pham";

export default function Service() {
  return (
    <section className="service">
      <Image
        // width={500}
        // height={500}
        className="img-03 common-img"
        // src="/assets/hq-images/chup-anh-san-pham/service-03.png"
        src={listImages.service03}
        placeholder="blur"
        alt="img-err"
        quality={100}
      />
      <Image
        width={500}
        height={500}
        className="img-04 common-img"
        // src="/assets/hq-images/chup-anh-san-pham/service-04.png"
        src={listImages.service04}
        placeholder="blur"
        alt="img-err"
        quality={100}
      />
      <Image
        width={500}
        height={500}
        className="img-05 common-img"
        // src="/assets/hq-images/chup-anh-san-pham/service-05.png"
        src={listImages.service05}
        placeholder="blur"
        alt="img-err"
        quality={100}
      />
      <Image
        width={500}
        height={500}
        className="img-06 common-img"
        // src="/assets/hq-images/chup-anh-san-pham/service-06.png"
        src={listImages.service06}
        placeholder="blur"
        alt="img-err"
        quality={100}
      />
      {/* <Image
        width={500}
        height={500}
        className="bg-img-02-service"
        src="/assets/hq-images/chup-anh-san-pham/service-mask-02.png"
        alt="img-err"
        quality={100}
      /> */}
      {/* <img
        className="img-03 common-img opti-image"
        data-src="./assets/hq-images/chup-anh-san-pham/service-03.png"
        src="./assets/images/chup-anh-san-pham/service-03.png"
        alt="img-err"
      />
      <img
        className="img-04 common-img opti-image"
        data-src="./assets/hq-images/chup-anh-san-pham/service-04.png"
        src="./assets/images/chup-anh-san-pham/service-04.png"
        alt="img-err"
      />
      <img
        className="img-05 common-img opti-image"
        data-src="./assets/hq-images/chup-anh-san-pham/service-05.png"
        src="./assets/images/chup-anh-san-pham/service-05.png"
        alt="img-err"
      />
      <img
        className="img-06 common-img opti-image"
        data-src="./assets/hq-images/chup-anh-san-pham/service-06.png"
        src="./assets/images/chup-anh-san-pham/service-06.png"
        alt="img-err"
      />
      <img
        className="bg-img-02-service"
        src="./assets/images/chup-anh-san-pham/service-mask-02.png"
        alt="img-err"
      /> */}
      <div className="container">
        <div className="service-container">
          <div className="img-01">
            <img
              src="./assets/images/chup-anh-san-pham/service-01.png"
              alt="img-err"
            />
          </div>
          <div className="service-text-img">
            <img
              src="./assets/images/chup-anh-san-pham/service-text.png"
              alt="img-err"
            />
          </div>
          <span className="txt-subhead">Từ thuê model, KOL, KOC,..</span>
          <div className="module-info-btn">
            {" "}
            <a
              className="btn-four style-pri openPopMona"
              href="https://mona.media/lien-he/"
              data-popup="solutions"
            >
              <span className="icon">
                {" "}
                <img
                  src="./assets/images/chup-anh-doanh-nhan/phone.png"
                  alt="img-err"
                />
              </span>
              <span className="txt">NHẬN TƯ VẤN TỪ ACCOUNT MONA</span>
            </a>
          </div>
          <img
            className="img-02 opti-image"
            data-src="./assets/hq-images/chup-anh-san-pham/service-02.png"
            src="./assets/images/chup-anh-san-pham/service-02.png"
            alt="img-err"
          />
        </div>
      </div>
    </section>
  );
}
