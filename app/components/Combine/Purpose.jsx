"use cient";
import Image from "next/image";
import listImages from "../../components/static/images/chup-anh-san-pham";

export default function Purpose() {
  return (
    <section className="sec-com purpose">
      <div className="container">
        <h2 className="txt-center txt-title">
          <div className="txt-head">
            MONA có{" "}
            <span className="txt-bg txt-bg-ani bg-lms add-active-js mg-auto span">
              <div className="txt"> đa dạng concept </div>
            </span>
          </div>
          <div className="txt-subhead">
            Đáp ứng tốt <span className="txt-lms"> với mọi mục đích </span>sử
            dụng <br />
            hình ảnh
          </div>
        </h2>
        <div className="purpose-container">
          <div className="swiper-purpose-js swiper">
            <div className="purpose-wrapper swiper-wrapper">
              <div className="purpose-slide swiper-slide">
                <div className="purpose-content--left">
                  <div className="txt-desc">
                    Ảnh chụp <span className="txt-decor"> với phông nền</span>
                  </div>
                  <ul className="purpose-list">
                    <li className="list-item">
                      <Image
                        width={50}
                        height={50}
                        className="check"
                        src="/assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        Chụp ảnh sản phẩm{" "}
                        <span className="txt-custom"> phông nền đơn sắc</span>
                        thường để tách nền và sử dụng vào thiết kế ấn phẩm tiếp
                        thị
                      </div>
                    </li>
                    <li className="list-item">
                      <Image
                        width={50}
                        height={50}
                        className="check"
                        src="/assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        <span className="txt-custom">
                          Hỗ trợ cung cấp đầy đủ{" "}
                        </span>{" "}
                        đạo cụ, thiết bị ánh sáng, phông nền phù hợp với sản
                        phẩm
                      </div>
                    </li>
                    <li className="list-item">
                      <Image
                        width={50}
                        height={50}
                        className="check"
                        src="/assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        <span className="txt-custom">Hướng dẫn tách nền</span>{" "}
                        trong bộ hướng dẫn sử dụng ảnh thông minh đi kèm
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="purpose-content--right">
                  <div className="purpose-contain-img">
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-01 img"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-01.jpg"
                      src={listImages.purposeSlide01}
                      placeholder="blur"
                      alt="img-err"
                      // style={{
                      //   width: "100%",
                      //   height: "auto",
                      // }}
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      placeholder="blur"
                      className="img-02 img"
                      src={listImages.purposeSlide02}
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      placeholder="blur"
                      className="img-03 img"
                      src={listImages.purposeSlide03}
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
              <div className="purpose-slide swiper-slide">
                <div className="purpose-content--left">
                  <div className="txt-desc">
                    Ảnh chụp{" "}
                    <span className="txt-decor"> có tiểu cảnh, concept</span>
                  </div>
                  <ul className="purpose-list">
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        Chụp sản phẩm
                        <span className="txt-custom"> có các tiểu cảnh </span>
                        theo từng loại sản phẩm
                      </div>
                    </li>
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        <span className="txt-custom">
                          Lên ý tưởng, stylist{" "}
                        </span>{" "}
                        hoặc theo ý tưởng của khách hàng
                      </div>
                    </li>
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        <span className="txt-custom">Hỗ trợ toàn bộ</span> dụng
                        cụ tạo tiểu cảnh, thiết bị ánh sáng, decor,...
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="purpose-content--right">
                  <div className="purpose-contain-img">
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-04 img"
                      placeholder="blur"
                      src={listImages.purposeSlide04}
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-04.jpg"
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-05 img"
                      src={listImages.purposeSlide05}
                      placeholder="blur"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-05.png"
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      placeholder="blur"
                      quality={100}
                      className="img-06 img"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-06.png"
                      src={listImages.purposeSlide06}
                      alt="img-err"
                    />
                  </div>
                </div>
              </div>
              <div className="purpose-slide swiper-slide">
                <div className="purpose-content--left">
                  <div className="txt-desc">
                    Ảnh chụp <span className="txt-decor"> với model</span>
                  </div>
                  <ul className="purpose-list">
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        Chụp ảnh sản phẩm
                        <span className="txt-custom"> có mẫu ảnh </span>
                      </div>
                    </li>
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        Thường áp dụng{" "}
                        <span className="txt-custom">
                          {" "}
                          chụp mỹ phẩm, sản phẩm làm đẹp, lookbook...
                        </span>
                      </div>
                    </li>
                    <li className="list-item">
                      <img
                        className="check"
                        src="./assets/images/chup-anh-san-pham/check-green.png"
                        alt="img-err"
                      />
                      <div className="txt-item">
                        <span className="txt-custom">
                          Hỗ trợ tìm mẫu nam & nữ phù hợp
                        </span>{" "}
                        với sản phẩm và mục đích truyền thông{" "}
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="purpose-content--right">
                  <div className="purpose-contain-img">
                    <Image
                      // width={500}
                      // height={500}
                      src={listImages.purposeSlide07}
                      placeholder="blur"
                      quality={100}
                      className="img-07 img"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-07.jpg"
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-08 img"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-08.jpg"
                      placeholder="blur"
                      src={listImages.purposeSlide08}
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-09 img"
                      placeholder="blur"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-09.jpg"
                      src={listImages.purposeSlide09}
                      alt="img-err"
                    />
                    <Image
                      // width={500}
                      // height={500}
                      quality={100}
                      className="img-10 img"
                      // src="/assets/hq-images/chup-anh-san-pham/purpose-slide-10.png"
                      src={listImages.purposeSlide10}
                      placeholder="blur"
                      alt="img-err"
                    />
                    {/* <img
                      className="img-07 img opti-image"
                      data-src="./assets/hq-images/chup-anh-san-pham/purpose-slide-07.jpg"
                      src="./assets/images/chup-anh-san-pham/purpose-slide-07.jpg"
                      alt="img-err"
                    />
                    <img
                      className="img-08 img opti-image"
                      data-src="./assets/hq-images/chup-anh-san-pham/purpose-slide-08.jpg"
                      src="./assets/images/chup-anh-san-pham/purpose-slide-08.jpg"
                      alt="img-err"
                    />
                    <img
                      className="img-09 img opti-image"
                      data-src="./assets/hq-images/chup-anh-san-pham/purpose-slide-09.jpg"
                      src="./assets/images/chup-anh-san-pham/purpose-slide-09.jpg"
                      alt="img-err"
                    />
                    <img
                      className="img-10 img opti-image"
                      data-src="./assets/hq-images/chup-anh-san-pham/purpose-slide-10.png"
                      src="./assets/images/chup-anh-san-pham/purpose-slide-10.png"
                      alt="img-err"
                    /> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="control-btn">
            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
          </div>
        </div>
      </div>
    </section>
  );
}
