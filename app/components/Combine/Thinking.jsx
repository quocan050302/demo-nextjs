"use client";

export default function Thinking() {
  return (
    <section className="sec-com thinking">
      <div className="container">
        <div className="thinking--inner">
          <div className="thinking-container">
            <img
              className="img-03 opti-image"
              data-src="./assets/hq-images/chup-anh-san-pham/thinking-img-03.png"
              src="./assets/images/chup-anh-san-pham/thinking-img-03.png"
              alt="img-err"
            />
            <img
              className="img-04"
              src="./assets/images/chup-anh-san-pham/thinking-img-04.png"
              alt="img-err"
            />
            <div className="thinking-content">
              <img
                className="img-01"
                src="./assets/images/chup-anh-san-pham/thinking-img.png"
                alt="img-err"
              />
              <img
                className="img-02"
                src="./assets/images/chup-anh-san-pham/thinking-img-02.png"
                alt="img-err"
              />
              <div className="thinking-txt">
                Kết hợp yếu tố con người vào hình ảnh{" "}
                <span className="txt-nhtq fw-700">
                  {" "}
                  giúp sản phẩm có tính ứng dụng cao
                </span>
                . Phô bày được kích thước, màu sắc, thành phần. Chọn những người
                mẫu nổi tiếng, có chuyên môn trong lĩnh vực sẽ
                <span className="txt-green fw-700">
                  {" "}
                  giúp sản phẩm bạn đáng tin cậy{" "}
                </span>
                vì sự nổi tiếng và uy tín của họ.
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="arrow-down-wrapper">
        <div className="arrow-group">
          <div className="arrow">
            {" "}
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
          <div className="arrow">
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
        </div>
        <div className="arrow-group">
          <div className="arrow">
            {" "}
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
          <div className="arrow">
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
        </div>
        <div className="arrow-group">
          <div className="arrow">
            {" "}
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
          <div className="arrow">
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
        </div>
        <div className="arrow-group">
          <div className="arrow">
            {" "}
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
          <div className="arrow">
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
        </div>
        <div className="arrow-group">
          <div className="arrow">
            {" "}
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
          <div className="arrow">
            <img src="./assets/images/tkw-never-stop/arrow-down.svg" alt="img-err" />
          </div>
        </div>
      </div>
    </section>
  );
}
