"use client";

import React from "react";
import Purpose from "./Purpose";
import Thinking from "./Thinking";
import Service from "./Service";
import Image from "next/image";

export default function Combine() {
  return (
    <div className="combine-three-section">
      <img
        className="bg-img-01"
        src="./assets/images/chup-anh-san-pham/service-bg.png"
        alt=""
      />
      <Purpose />
      <Thinking />
      <Service />
    </div>
  );
}
