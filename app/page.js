

import Image from "next/image";
import Script from "next/script";
import Link from "next/link";
import { TabGroup } from "./TabGroup/tab-group";
import Hydration from "./client/Hydration";
import ListCustomer from "./components/ListCustomer";
import Combine_head from "./components/Combine-head-section/Combine_head"
import Combine_cm_mf from "./components/Combine-Cm-Mf/Combine_cm_mf"
import Team from "./components/Team";
import Module from "./components/Module";
import Commit from "./components/Commit";
import Feedback from "./components/Feedback";
import Quality from "./components/Quality";
import Module_Professtional from "./components/Module_Professional"
import Company from "./components/Company"
import FAQ from "./components/FAQ"



export default function Home() {
    const ids = [{ id: "1" }, { id: "2" }, { id: "3" }];
    return (
        <div>
            <Combine_head />
            {/* <ListCustomer /> */}
            {/* <Combine_cm_mf /> */}
            {/* <Team /> */}
            {/* <Module /> */}
            {/* <Commit /> */}
            {/* <Feedback /> */}
            {/* <Quality /> */}
            {/* <Module_Professtional /> */}
            {/* <Company />
            <FAQ /> */}
            <Hydration />

            <Script id='common1'>
                {`
        // START Add active
        const configObserver = {
            rootMargin: '-50px -50px -50px -50px',
            threshold: [0, 0.25, 0.75, 1],
        };
        
        const addActives = document.querySelectorAll('.add-active-js');
        
        const observerAddActives = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.intersectionRatio > 0) {
                    entry.target.classList?.add('active');
                } else {
                    if (!entry.target.classList.contains('loader')) {
                        entry.target.classList.remove('active');
                    }
                }
            });
        }, configObserver);
        
        addActives.forEach((ele) => {
            observerAddActives.observe(ele);
        });
        // END Add active
        
        function reveal() {
            var reveals = document.querySelectorAll(".ani");
            for (var i = 0; i < reveals.length; i++) {
                var windowHeight = window.innerHeight;
                var elementTop = reveals[i].getBoundingClientRect().top;
                var elementVisible = 50;
        
                if (elementTop < windowHeight - elementVisible) {
                    reveals[i].classList?.add("active");
                } else {
                    // reveals[i].classList.remove("active");
                }
            }
        }
        reveal();
        window.addEventListener("scroll", reveal);
        
        function cscrollAddClass(el, className) {
            document.querySelectorAll(el).forEach(function(item) {
                let itemTop = item.offsetTop;
                if (
                    item.classList.contains("custom-fadeInUpBig") ||
                    item.classList.contains("custom-bounceInUp")
                ) {
                    itemTop -= 2000;
                }
                if (
                    item.classList.contains("custom-fadeInDownBig") ||
                    item.classList.contains("custom-bounceInDown")
                ) {
                    itemTop += 2000;
                }
                if (itemTop < window.scrollY + window.innerHeight / 10 * 8) {
                    item.classList?.add(className);
                }
            });
        }
        
        function cbindImageAnimations() {
            cscrollAddClass(".scr-item", "active");
            window.addEventListener("scroll", function() {
                cscrollAddClass(".scr-item", "active");
            });
        }
        cbindImageAnimations();
        const addActivesss = document.querySelectorAll('.factor-san-pham');

const observerAddActivesss = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.intersectionRatio > 0) {
            var progressCircles = document.querySelectorAll('.c-progress-circle');

            setTimeout(function () {
                for (var i = 0; i < progressCircles.length; i++) {
                    var circle = progressCircles[i],
                        val = Number(circle.getAttribute('data-percentage')),
                        bar = circle.querySelectorAll('.c-progress-circle__bar')[0];
                    if (isNaN(val)) {
                        val = 100;
                    }
                    else {
                        var r = bar.getAttribute('r');
                        var c = Math.PI * (r * 2);

                        if (val < 0) { val = 0; }
                        if (val > 100) { val = 100; }

                        var pct = ((100 - val) / 100) * c;

                        bar.style.strokeDashoffset = pct;
                        bar.setAttribute('data-percentage', val);
                    }
                }
            }, 300);
        }
    });
}, configObserver);

addActivesss.forEach(ele => {
    observerAddActivesss.observe(ele);
});
const addActivess = document.querySelectorAll('.count-block');

const observerAddActivess = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.intersectionRatio > 0) {
            let counterNum = entry.target.querySelectorAll(".counter-up");
            if (counterNum.length > 0) {
                counterNum.forEach((ele) => {
                    let countTo = ele.getAttribute("data-count");
                    createCountUpElement(ele, 0, countTo, 60, '.')
                })
            }
        }
    });
}, configObserver);

addActivess.forEach(ele => {
    console.log(ele)
    observerAddActivess.observe(ele);
});

// Function to create and animate a count-up element with an optional separator
// duration is frame => second * 30 
// end must be int
function createCountUpElement(element, start, end, duration, separator = "") {

    if (element.textContent != '0') {
        return 0;
    }
    let current = start;
    end = parseInt(end);
    const step = (end - start) / duration;

    function updateValue() {
        // Display the value with the separator, if provided
        element.textContent = separator ? numberWithSeparator(current, separator) : current;
        current += step;

        if (current <= end) {
            requestAnimationFrame(updateValue);
        } else {
            // Display the end value with the separator, if provided
            element.textContent = separator ? numberWithSeparator(end, separator) : end;
        }
    }

    updateValue();
}

// Function to format a number with a custom separator
function numberWithSeparator(number, separator) {
    return number.toLocaleString(undefined, {
        useGrouping: true,
        minimumFractionDigits: 0,
        maximumFractionDigits: 0, // Increase to allow for more decimal places
    }).replace(/,/g, separator);
}
        `}
            </Script>
        </div>
    );
}
