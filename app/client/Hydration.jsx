"use client";
import React, { useEffect } from "react";
import Swiper from "../../public/libs/swiper/swiper-bundle.min.js";
import lightGallery from "../../public/libs/lightgallery-new/lightgallery.min.js";
import lgThumbnail from "../../public/libs/lightgallery-new/lg-thumbnail.min.js";
export default function Hydration() {
  useEffect(() => {
    //currentWidth has been declared in other file
    const galleryThumbsCommit = new Swiper(".benefit .slide-text-show", {
      spaceBetween: 10,
      slidesPerView: "auto",
      direction: "vertical",
      allowTouchMove: false,
    });
    const galleryTopCommit = new Swiper(".benefit .swiper-banner", {
      // spaceBetween: currentWidth <= 576 ? 16 : currentWidth <= 768 ? 24 : 30,
      grabCursor: true,
      speed: 500,
      slidesPerView: window.innerWidth <= 768 ? 1.2 : 1,

      thumbs: {
        swiper: galleryThumbsCommit,
      },
      on: {
        init: function () {
          const current = this.realIndex;
          // adjustOpacity(current);
        },
        slideChange: function () {
          const current = this.realIndex;
          // adjustOpacity(current);
        },
      },
    });
    // Js get height item

    function getHeight() {
      const getHeightItems = document.querySelectorAll(".getHeight");
      if (getHeightItems) {
        getHeightItems.forEach((item) => {
          item.style = `--height:${item.getBoundingClientRect().height}px`;
        });
      }
    }
    getHeight();

    // Js get width item
    function getWidth() {
      const getWidthItems = document.querySelectorAll(".getWidth");
      if (getWidthItems) {
        getWidthItems.forEach((item) => {
          item.style = `--width:${item.getBoundingClientRect().width}px`;
        });
      }
    }
    getWidth();
    window.addEventListener("resize", () => {
      getHeight();
      getWidth();
    });
  }, []);
  useEffect(() => {
    if (window.innerWidth > 769) {
      const swiperCommit = document.querySelector(".swiper-commit");
      if (swiperCommit) {
        new Swiper(swiperCommit, {
          slidesPerView: 1,
          spaceBetween: 24,
          centeredSlides: true,
          pagination: {
            el: ".commit-content .swiper-pagination",
            clickable: true,
          },
          navigation: {
            nextEl: ".commit-content .swiper-button-next",
            prevEl: ".commit-content .swiper-button-prev",
          },
        });
      }
    }
    const swiperPurpose = document.querySelector(".swiper-purpose-js");
    if (swiperPurpose && window.innerWidth > 768) {
      new Swiper(swiperPurpose, {
        slidesPerView: "auto",
        // spaceBetween: 16,
        grabCursor: true,
        speed: 800,
        // centeredSlides: true,
        // loop: true,
        navigation: {
          nextEl: ".purpose .swiper-button-next",
          prevEl: ".purpose .swiper-button-prev",
        },
      });
    }

    let Gal = document.querySelectorAll(".gallery");
    let noThumbs = document.querySelector(".web-demo-cate-btn .button");
    if (noThumbs) {
      noThumbs = false;
    }
    if (Gal.length > 0) {
      Gal.forEach((ele) => {
        lightGallery(ele, {
          selector: ".gallery__img",
          exThumbImage: "data-src",
          autoplayControls: false,
          flipHorizontal: false,
          flipVertical: false,
          rotate: false,
          share: false,
          fullScreen: false,
          actualSize: false,
          download: false,
          thumbnail: true,
          plugins: [lgThumbnail],
          mobileSettings: { controls: true, showCloseIcon: true },
        });
      });
    }

    let triggers = document.querySelectorAll(".trigger-gallery-js");
    if (triggers.length > 0) {
      for (let trigger of triggers) {
        trigger.addEventListener("click", (e) => {
          let wrapper = trigger.closest(".trigger-wrap-js");
          if (wrapper) {
            let target_item = wrapper.querySelector(".gallery__img");
            if (target_item) target_item.click();
          }
        });
      }
    }
  }, []);

  return <div></div>;
}
